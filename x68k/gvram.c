// ---------------------------------------------------------------------------------------
//  GVRAM.C - Graphic VRAM
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	"windraw.h"
#include	"winx68k.h"
#include	"crtc.h"
#include	"palette.h"
#include	"tvram.h"
#include	"gvram.h"
#include	"m68000.h"
#include	"memory.h"

	BYTE	GVRAM[0x80000];
	WORD	Grp_LineBuf[1024];
	WORD	Grp_LineBufSP[1024];		// 特殊プライオリティ／半透明用バッファ
	WORD	Grp_LineBufSP2[1024];		// 半透明ベースプレーン用バッファ（非半透明ビット格納）

	WORD	Pal16Adr[256];			// 16bit color パレットアドレス計算用


// -----------------------------------------------------------------------
//   初期化〜
// -----------------------------------------------------------------------
void GVRAM_Init(void)
{
	int i;

	ZeroMemory(GVRAM, 0x80000);
	for (i=0; i<128; i++)			// 16bit color パレットアドレス計算用
	{
		Pal16Adr[i*2] = i*4;
		Pal16Adr[i*2+1] = i*4+1;
	}
}


// -----------------------------------------------------------------------------------
//  高速クリア用ルーチン
// -----------------------------------------------------------------------------------

void FASTCALL GVRAM_FastClear(void)
{
	DWORD v, h;
	v = ((CRTC_Regs[0x29]&4)?512:256);
	h = ((CRTC_Regs[0x29]&3)?512:256);
	// やっぱちゃんと範囲指定しないと変になるものもある（ダイナマイトデュークとか）
	_asm
	{
		push	ebx
		push	ecx
		push	edx
		push	esi
		mov	ax, CRTC_FastClrMask
		mov	ecx, v
		mov	esi, GrphScrollY[0]
		and	esi, 511
		shl	esi, 10
	fclp2:
		mov	edx, GrphScrollX[0]
		and	edx, 511
		mov	ebx, h
	fclp:
		and	word ptr GVRAM[esi+edx*2], ax
		inc	edx
		and	edx, 511
		dec	ebx
		jne	fclp
		add	esi, 1024
		and	esi, 07fc00h
		dec	ecx
		jne	fclp2
		pop	esi
		pop	edx
		pop	ecx
		pop	ebx
	}
}


// -----------------------------------------------------------------------
//   VRAM Read
// -----------------------------------------------------------------------
BYTE FASTCALL GVRAM_Read(DWORD adr)
{
	BYTE ret=0;
	BYTE page;
	WORD *ram = (WORD*)(&GVRAM[adr&0x7fffe]);
	adr ^= 1;
	adr -= 0xc00000;

	if (CRTC_Regs[0x28]&8) {			// 読み込み側も65536モードのVRAM配置（苦胃頭捕物帳）
		if (adr<0x80000) ret = GVRAM[adr];
	} else {
		switch(CRTC_Regs[0x28]&3)
		{
		case 0:					// 16 colors
			if (!(adr&1))
			{
				if (CRTC_Regs[0x28]&4)		// 1024dot
				{
					ram = (WORD*)(&GVRAM[((adr&0xff800)>>1)+(adr&0x3fe)]);
					page = (BYTE)((adr>>17)&0x08);
					page += (BYTE)((adr>>8)&4);
					ret = (((*ram)>>page)&15);
				}
				else
				{
					page = (BYTE)((adr>>17)&0x0c);
					ret = (((*ram)>>page)&15);
				}
			}
			break;
		case 1:					// 256
		case 2:					// Unknown
			if ( adr<0x100000 )
			{
				if (!(adr&1))
				{
					page = (BYTE)((adr>>16)&0x08);
					ret = (BYTE)((*ram)>>page);
				}
			}
//			else
//				BusErrFlag = 1;
			break;
		case 3:					// 65536
			if (adr<0x80000)
				ret = GVRAM[adr];
//			else
//				BusErrFlag = 1;
			break;
		}
	}
	return ret;
}


// -----------------------------------------------------------------------
//   VRAM Write
// -----------------------------------------------------------------------
void FASTCALL GVRAM_Write(DWORD adr, BYTE data)
{
	BYTE page;
	int line = 1023, scr = 0;
	WORD *ram = (WORD*)(&GVRAM[adr&0x7fffe]);
	WORD temp;

	adr ^= 1;
	adr -= 0xc00000;


	if (CRTC_Regs[0x28]&8)				// 65536モードのVRAM配置？（Nemesis）
	{
		if ( adr<0x80000 )
		{
			GVRAM[adr] = data;
			line = (((adr&0x7ffff)/1024)-GrphScrollY[0])&511;
		}
	}
	else
	{
		switch(CRTC_Regs[0x28]&3)
		{
		case 0:					// 16 colors
			if (adr&1) break;
			if (CRTC_Regs[0x28]&4)		// 1024dot
			{
				ram = (WORD*)(&GVRAM[((adr&0xff800)>>1)+(adr&0x3fe)]);
				page = (BYTE)((adr>>17)&0x08);
				page += (BYTE)((adr>>8)&4);
				temp = ((WORD)data&15)<<page;
				*ram = ((*ram)&(~(0xf<<page)))|temp;
				line = ((adr/2048)-GrphScrollY[0])&1023;
			}
			else
			{
				page = (BYTE)((adr>>17)&0x0c);
				temp = ((WORD)data&15)<<page;
				*ram = ((*ram)&(~(0xf<<page)))|temp;
				switch(adr/0x80000)
				{
					case 0:	scr = GrphScrollY[0]; break;
					case 1: scr = GrphScrollY[1]; break;
					case 2: scr = GrphScrollY[2]; break;
					case 3: scr = GrphScrollY[3]; break;
				}
				line = (((adr&0x7ffff)/1024)-scr)&511;
			}
			break;
		case 1:					// 256 colors
		case 2:					// Unknown
			if ( adr<0x100000 )
			{
				if ( !(adr&1) )
				{
					scr = GrphScrollY[(adr>>18)&2];
					line = (((adr&0x7ffff)>>10)-scr)&511;
					TextDirtyLine[line] = 1;			// 32色4面みたいな使用方法時
					scr = GrphScrollY[((adr>>18)&2)+1];		//
					line = (((adr&0x7ffff)>>10)-scr)&511;		//
					if (adr&0x80000) adr+=1;
					adr &= 0x7ffff;
					GVRAM[adr] = data;
				}
			}
//			else
//			{
//				BusErrFlag = 1;
//				return;
//			}
			break;
		case 3:					// 65536 colors
			if ( adr<0x80000 )
			{
				GVRAM[adr] = data;
/*{
FILE* fp = fopen("_gvram.txt", "a");
fprintf(fp, "Adr:$%08X Dat:$%02X @ $%08X\n", (adr^1)+0xc00000, data, regs.pc);
fclose(fp);
}*/
				line = (((adr&0x7ffff)>>10)-GrphScrollY[0])&511;
			}
//			else
//			{
//				BusErrFlag = 1;
//				return;
//			}
			break;
		}
		TextDirtyLine[line] = 1;
	}
}


// -----------------------------------------------------------------------
//   こっから後はライン単位での画面展開部
// -----------------------------------------------------------------------
LABEL void Grp_DrawLine16(void)
{
	__asm {
			pushf
			cld
			push	es
			mov	ax, ds
			mov	es, ax
			push	ebx
			push	esi
			push	edi
			mov	esi, GrphScrollY[0]
			add	esi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp16linenotspecial
			add	esi, VLINE
		gp16linenotspecial:
			and	esi, 511
			shl	esi, 10
			mov	ebx, GrphScrollX[0]
			and	ebx, 511
			lea	esi, GVRAM[esi+ebx*2]
			xor	ebx, 511
			inc	bx
			mov	ecx, TextDotX
			mov	edi, offset Grp_LineBuf
			xor	eax, eax
			xor	edx, edx
			cmp	ecx, ebx
			jbe	gp16linelp_b
			sub	ecx, ebx
		gp16linelp_a:
			lodsw
			or	ax, ax
			je	gp16linelp_a_skip
			mov	dl, ah
			mov	ah, 0
			mov	dh, 0
			mov	ax, word ptr Pal16Adr[eax*2]
			mov	dx, word ptr Pal16Adr[edx*2]
			mov	al, byte ptr Pal_Regs[eax]
			mov	ah, byte ptr Pal_Regs[edx+2]
			mov	ax, word ptr Pal16[eax*2]
;or ax, Ibit		; 20010120
		gp16linelp_a_skip:
			stosw
			dec	bx
			jnz	gp16linelp_a
			sub	esi, 400h
		gp16linelp_b:
			lodsw
			or	ax, ax
			je	gp16linelp_b_skip
			mov	dl, ah
			mov	ah, 0
			mov	dh, 0
			mov	ax, word ptr Pal16Adr[eax*2]
			mov	dx, word ptr Pal16Adr[edx*2]
			mov	al, byte ptr Pal_Regs[eax]
			mov	ah, byte ptr Pal_Regs[edx+2]
			mov	ax, word ptr Pal16[eax*2]
;or ax, Ibit		; 20010120
		gp16linelp_b_skip:
			stosw
			loop	gp16linelp_b
			pop	edi
			pop	esi
			pop	ebx
			pop	es
			popf
			ret
	}
}


LABEL void FASTCALL Grp_DrawLine8(int page, int opaq)
{
	__asm {
			pushf
			cld
			push	ebx
			push	ecx
			push	esi
			push	edi

			and	ecx, 1		// ecx = page
			mov	esi, GrphScrollY[ecx*8]
			mov	edi, GrphScrollY[ecx*8+4]
			add	esi, VLINE
			add	edi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp8linenotspecial
			add	esi, VLINE
			add	edi, VLINE
		gp8linenotspecial:
			and	esi, 511
			shl	esi, 10
			add	esi, ecx
			and	edi, 511
			shl	edi, 10
			add	edi, ecx
			mov	ebx, GrphScrollX[ecx*8+4]
			and	ebx, 511
			add	edi, ebx
			add	edi, ebx
			mov	ebx, GrphScrollX[ecx*8]
			and	ebx, 511
			lea	esi, GVRAM[esi+ebx*2]
			xor	ebx, 511
			inc	bx

			xor	eax, eax
			mov	ecx, TextDotX

			or	edx, edx	// edx = opaq
			mov	edx, offset Grp_LineBuf
			je	gp8linelp

			cmp	ecx, ebx
			jbe	gp8olinelp_b
			sub	ecx, ebx

		gp8olinelp_a:
			lodsw
			mov ah, byte ptr GVRAM[edi]
			and ah, 0f0h
			and al, 0fh
			or al, ah
			mov	ah, 0
;or al,al
			mov	ax, word ptr GrphPal[eax*2]
;jz gp8noti_a
;or ax, Ibit		; 20010120
;gp8noti_a:
			mov	[edx], ax
			add	edx, 2
			add	edi, 2
			test	di, 03feh
			jnz	gp8onotxend1
			sub	edi, 0400h
		gp8onotxend1:
			dec	bx
			jnz	gp8olinelp_a
			sub	esi, 400h
		gp8olinelp_b:
			lodsw
			mov ah, byte ptr GVRAM[edi]
			and ah, 0f0h
			and al, 0fh
			or al, ah
			mov	ah, 0
;or al,al
			mov	ax, word ptr GrphPal[eax*2]
;jz gp8noti_b
;or ax, Ibit		; 20010120
;gp8noti_b:
			mov	[edx], ax
			add	edx, 2
			add	edi, 2
			test	di, 03feh
			jnz	gp8onotxend2
			sub	edi, 0400h
		gp8onotxend2:
			loop	gp8olinelp_b

			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
			popf
			ret

		gp8linelp:
			cmp	ecx, ebx
			jbe	gp8linelp_b
			sub	ecx, ebx
		gp8linelp_a:
			lodsw
			mov ah, byte ptr GVRAM[edi]
			and ah, 0f0h
			and al, 0fh
			or al, ah
			and	ax, 00ffh
			jz	gp8lineskip_a
			mov	ax, word ptr GrphPal[eax*2]
;or ax, Ibit		; 20010120
			mov	[edx], ax
		gp8lineskip_a:
			add	edx, 2
			add	edi, 2
			test	di, 03feh
			jnz	gp8notxend1
			sub	edi, 0400h
		gp8notxend1:
			dec	bx
			jnz	gp8linelp_a
			sub	esi, 400h
		gp8linelp_b:
			lodsw
			mov ah, byte ptr GVRAM[edi]
			and ah, 0f0h
			and al, 0fh
			or al, ah
			and	ax, 00ffh
			jz	gp8lineskip_b
			mov	ax, word ptr GrphPal[eax*2]
;or ax, Ibit		; 20010120
			mov	[edx], ax
		gp8lineskip_b:
			add	edx, 2
			add	edi, 2
			test	di, 03feh
			jnz	gp8notxend2
			sub	edi, 0400h
		gp8notxend2:
			loop	gp8linelp_b

			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
			popf
			ret
	}
}

				// Manhattan Requiem Opening 7.0→7.5MHz
LABEL void FASTCALL Grp_DrawLine4(DWORD page, int opaq)
{
	__asm {
			pushf
			cld
			push	ebx
			push	ecx
			push	esi
			and	ecx, 3
			mov	esi, GrphScrollY[ecx*4]
			add	esi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp4linenotspecial
			add	esi, VLINE
		gp4linenotspecial:
			and	esi, 511
			shl	esi, 10
			mov	ebx, GrphScrollX[ecx*4]
			and	ebx, 511
			lea	esi, [esi+ebx*2]
			xor	ebx, 511
			inc	bx
			xor	eax, eax

			shr	cl, 1
			lea	esi, GVRAM[esi+ecx]	// ecx = page/2
			jnc	gp4olinepage0		// (shr cl,1) page0or2

			mov	ecx, TextDotX
			or	edx, edx
			mov	edx, offset Grp_LineBuf
			jz	gp4linelp2		// opaq == 0

			cmp	ecx, ebx
			jbe	gp4olinelp2_b
			sub	ecx, ebx
		gp4olinelp2_a:
			lodsw
			mov	ah, 0
			shr	al, 4
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
			add	edx, 2
			dec	bx
			jnz	gp4olinelp2_a
			sub	esi, 400h
		gp4olinelp2_b:
			lodsw
			mov	ah, 0
			shr	al, 4
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
			add	edx, 2
			loop	gp4olinelp2_b
			pop	esi
			pop	ecx
			pop	ebx
			popf
			ret

		gp4linelp2:
			cmp	ecx, ebx
			jbe	gp4linelp2_b
			sub	ecx, ebx
		gp4linelp2_a:
			lodsw
			mov	ah, 0
			shr	al, 4			// shrのZFは確認済
			jz	gp4lineskip2_a
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
		gp4lineskip2_a:
			add	edx, 2
			dec	bx
			jnz	gp4linelp2_a
			sub	esi, 400h
		gp4linelp2_b:
			lodsw
			mov	ah, 0
			shr	al, 4			// shrのZFは確認済
			jz	gp4lineskip2_b
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
		gp4lineskip2_b:
			add	edx, 2
			loop	gp4linelp2_b
			pop	esi
			pop	ecx
			pop	ebx
			popf
			ret

		gp4olinepage0:
			mov	ecx, TextDotX
			or	edx, edx
			mov	edx, offset Grp_LineBuf
			jz	gp4linelp		// opaq == 0

			cmp	ecx, ebx
			jbe	gp4olinelp_b
			sub	ecx, ebx
		gp4olinelp_a:
			lodsw
			and	ax, 15
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
			add	edx, 2
			dec	bx
			jnz	gp4olinelp_a
			sub	esi, 400h
		gp4olinelp_b:
			lodsw
			and	ax, 15
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
			add	edx, 2
			loop	gp4olinelp_b
			pop	esi
			pop	ecx
			pop	ebx
			popf
			ret

		gp4linelp:
			cmp	ecx, ebx
			jbe	gp4linelp_b
			sub	ecx, ebx
		gp4linelp_a:
			lodsw
			and	ax, 15
			jz	gp4lineskip_a
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
		gp4lineskip_a:
			add	edx, 2
			dec	bx
			jnz	gp4linelp_a
			sub	esi, 400h
		gp4linelp_b:
			lodsw
			and	ax, 15
			jz	gp4lineskip_b
			mov	ax, word ptr GrphPal[eax*2]
			mov	[edx], ax
		gp4lineskip_b:
			add	edx, 2
			loop	gp4linelp_b
			pop	esi
			pop	ecx
			pop	ebx
			popf
			ret
	}
}

					// この画面モードは勘弁して下さい…
void FASTCALL Grp_DrawLine4h(void)
{
	__asm
	{
		push	esi
		push	edi
		mov	esi, GrphScrollY[0]
		add	esi, VLINE
		mov	al, CRTC_Regs[0x29]
		and	al, 1ch
		cmp	al, 1ch
		jne	gp4hlinenotspecial
		add	esi, VLINE
	gp4hlinenotspecial:
		and	esi, 1023
		test	esi, 512
		jnz	gp4h_plane23
		shl	esi, 10
		mov	edx, GrphScrollX[0]
		mov	edi, edx
		and	edx, 511
		add	esi, edx
		add	esi, edx
		mov	cl, 00h
		test	edi, 512
		jz	gp4h_main
		add	cl, 4
		jmp	gp4h_main
	gp4h_plane23:
		and	esi, 511
		shl	esi, 10
		mov	edx, GrphScrollX[0]
		mov	edi, edx
		and	edx, 511
		add	esi, edx
		add	esi, edx
		mov	cl, 08h
		test	edi, 512
		jz	gp4h_main
		add	cl, 4
	gp4h_main:
		and	edi, 511
		xor	di, 511
		inc	di
		//and	di, 512
		mov	ebx, TextDotX
		xor	edx, edx
	gp4hlinelp:
		mov	ax, word ptr GVRAM[esi]
		shr	ax, cl
		and	eax, 15
		mov	ax, word ptr GrphPal[eax*2]
		mov	Grp_LineBuf[edx], ax
		add	esi, 2
		add	edx, 2
		dec	di
		jnz	gp4hline_cnt
		sub	esi, 0400h
		xor	cl, 4
		mov	di, 512
	gp4hline_cnt:
		dec	bx
		jnz	gp4hlinelp
//		loop	gp4hlinelp
		pop	edi
		pop	esi
	}
}


// -------------------------------------------------
// --- 半透明／特殊Priのベースとなるページの描画 ---
// -------------------------------------------------
void FASTCALL Grp_DrawLine16SP(void)
{
	__asm
	{
		push	edi
		push	esi
		mov	edi, 0
		mov	esi, GrphScrollY[0]
		add	esi, VLINE
		mov	al, CRTC_Regs[0x29]
		and	al, 1ch
		cmp	al, 1ch
		jne	gp16splinenotspecial
		add	esi, VLINE
	gp16splinenotspecial:
		and	esi, 511
		shl	esi, 10
		mov	ebx, GrphScrollX[0]
		and	ebx, 511
		add	esi, ebx
		add	esi, ebx
		xor	ebx, 511
		inc	ebx
		mov	ecx, TextDotX
		xor	edx, edx
	gp16splinelp:
		movzx	eax, byte ptr GVRAM[esi+1]
		mov	dh, byte ptr Pal_Regs[eax*2]
		movzx	eax, byte ptr GVRAM[esi]
		mov	dl, byte ptr Pal_Regs[eax*2+1]
		test	al, 1
		jnz	gp16splinesp
		and	dx, 0fffeh
		mov	ax, word ptr Pal16[edx*2]
		mov	word ptr Grp_LineBufSP[edi], 0
		mov	Grp_LineBufSP2[edi], ax
		jmp	gp16splineskip
	gp16splinesp:
		and	dx, 0fffeh
		mov	ax, word ptr Pal16[edx*2]
		mov	word ptr Grp_LineBufSP2[edi], 0
		mov	word ptr Grp_LineBufSP[edi], ax
	gp16splineskip:
		add	esi, 2
		add	edi, 2
		dec	ebx
		jnz	gp16spline_cnt
		sub	esi, 0400h
	gp16spline_cnt:
//		dec	cx
//		jnz	gp16splinelp
		loop	gp16splinelp
		pop	esi
		pop	edi
	}
}


void FASTCALL Grp_DrawLine8SP(int page)
{
		__asm
		{
			push	esi
push	edi
and	ecx, 1
mov	esi, GrphScrollY[ecx*8]
mov	edi, GrphScrollY[ecx*8+4]
			add	esi, VLINE
add	edi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp8splinenotspecial
			add	esi, VLINE
add	edi, VLINE
		gp8splinenotspecial:
			and	esi, 511
			shl	esi, 10
and	edi, 511
shl	edi, 10
mov	eax, GrphScrollX[ecx*8+4]
and	eax, 511
add	edi, eax
add	edi, eax
			mov	ebx, GrphScrollX[ecx*8]
			and	ebx, 511
			add	esi, ebx
			add	esi, ebx
add	esi, ecx
add	edi, ecx

			xor	bx, 511
			inc	bx
			mov	ecx, TextDotX
			xor	edx, edx
			xor	eax, eax
		gp8osplinelp:
			mov	al, byte ptr GVRAM[esi]
mov ah, byte ptr GVRAM[edi]
and ah, 0f0h
and al, 0fh
or al, ah
			//xor	ah, ah
			test	al, 1
			jnz	gp8osplinesp
			and	eax, 0feh
			jz	gp8onotzero2
			mov	ax, word ptr GrphPal[eax*2]
		gp8onotzero2:
			mov	word ptr Grp_LineBufSP[edx], 0
			mov	word ptr Grp_LineBufSP2[edx], ax
			jmp	gp8osplineskip
		gp8osplinesp:
			and	eax, 0feh
			jz	gp8onotzero			// ついんびー。Palette0以外の$0000は特殊Priでは透明じゃなく黒扱いらしい
			mov	ax, word ptr GrphPal[eax*2]
			or	ax, Ibit			// Palette0以外はIbit立ててごまかそー
		gp8onotzero:					// 半透明が変になる時は、半透明と特殊Priを別ルーチンにしなきゃ…
			mov	word ptr Grp_LineBufSP[edx], ax
			mov	word ptr Grp_LineBufSP2[edx], 0
		gp8osplineskip:
			add	esi, 2
add	edi, 2
test	di, 03feh
jnz	gp8spnotxend
sub	edi, 0400h
gp8spnotxend:
			add	edx, 2
			dec	bx
			jnz	gp8ospline_cnt
			sub	esi, 0400h
		gp8ospline_cnt:
			loop	gp8osplinelp
pop	edi
			pop	esi
		}
}


void FASTCALL Grp_DrawLine4SP(DWORD page/*, int opaq*/)
{
	DWORD scrx, scry;
	switch(page)		// 美しくなさすぎる（笑）
	{
	case 0:	scrx = GrphScrollX[0]; scry = GrphScrollY[0]; break;
	case 1: scrx = GrphScrollX[1]; scry = GrphScrollY[1]; break;
	case 2: scrx = GrphScrollX[2]; scry = GrphScrollY[2]; break;
	case 3: scrx = GrphScrollX[3]; scry = GrphScrollY[3]; break;
	}

	if (page&1)
	{
//	if (opaq)
//	{
		__asm
		{
			push	esi
			mov	esi, scry
			add	esi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp4splinenotspecial
			add	esi, VLINE
		gp4splinenotspecial:
			and	esi, 511
			shl	esi, 10
			mov	ebx, scrx
			and	ebx, 511
			add	esi, ebx
			add	esi, ebx
//			add	esi, vram
			test	page, 2
			jz	gp4ospline2page0
			inc	esi
		gp4ospline2page0:
			xor	bx, 511
			inc	bx
			mov	ecx, TextDotX
			xor	edx, edx
		gp4osplinelp2:
			mov	al, byte ptr GVRAM[esi]
			shr	al, 4
			test	al, 1
			jnz	gp4o2splinesp
			and	eax, 14
			mov	ax, word ptr GrphPal[eax*2]
			mov	word ptr Grp_LineBufSP[edx], 0
			mov	Grp_LineBufSP2[edx], ax
			jmp	gp4o2splineskip
		gp4o2splinesp:
			and	eax, 14
			mov	ax, word ptr GrphPal[eax*2]
			mov	word ptr Grp_LineBufSP[edx], ax
			mov	word ptr Grp_LineBufSP2[edx], 0
		gp4o2splineskip:
			add	esi, 2
			add	edx, 2
			dec	bx
			jnz	gp4ospline_cnt2
			sub	esi, 0400h
		gp4ospline_cnt2:
//			dec	cx
//			jnz	gp4osplinelp2
			loop	gp4osplinelp2
			pop	esi
		}
/*	}
	else
	{
		__asm
		{
			push	esi
			mov	esi, scry
			add	esi, VLINE
			and	esi, 511
			shl	esi, 10
			mov	ebx, scrx
			and	ebx, 511
			add	esi, ebx
			add	esi, ebx
//			add	esi, vram
			test	page, 2
			jz	gp4spline2page0
			inc	esi
		gp4spline2page0:
			xor	bx, 511
			inc	bx
			mov	ecx, TextDotX
			xor	edx, edx
		gp4splinelp2:
			mov	al, byte ptr GVRAM[esi]
			shr	al, 4
			test	al, 1
			jnz	gp4splinesp2
			and	eax, 14
			jz	gp4splineskip2_2
			mov	ax, word ptr GrphPal[eax*2]
			mov	Grp_LineBuf[edx], ax
		gp4splineskip2_2:
			mov	word ptr Grp_LineBufSP[edx], 0
			jmp	gp4splineskip2
		gp4splinesp2:
			and	eax, 14
			jz	gp4splineskip2_2
			mov	ax, word ptr GrphPal[eax*2]
//			mov	Grp_LineBuf[edx], ax
			mov	word ptr Grp_LineBufSP[edx], ax
		gp4splineskip2:
			add	esi, 2
			add	edx, 2
			dec	bx
			jnz	gp4spline_cnt2
			sub	esi, 0400h
		gp4spline_cnt2:
//			dec	cx
//			jnz	gp4splinelp2
			loop	gp4splinelp2
			pop	esi
		}
	}
*/
	}
	else
	{
//	if (opaq)
//	{
		__asm
		{
			push	esi
			mov	esi, scry
			add	esi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp4osplinenotspecial
			add	esi, VLINE
		gp4osplinenotspecial:
			and	esi, 511
			shl	esi, 10
			mov	ebx, scrx
			and	ebx, 511
			add	esi, ebx
			add	esi, ebx
//			add	esi, vram
			test	page, 2
			jz	gp4osplinepage0
			inc	esi
		gp4osplinepage0:
			xor	bx, 511
			inc	bx
			mov	ecx, TextDotX
			xor	edx, edx
		gp4osplinelp:
			mov	al, byte ptr GVRAM[esi]
			test	al, 1
			jnz	gp4osplinesp
			and	eax, 14
			mov	ax, word ptr GrphPal[eax*2]
			mov	word ptr Grp_LineBufSP[edx], 0
			mov	Grp_LineBufSP2[edx], ax
			jmp	gp4osplineskip
		gp4osplinesp:
			and	eax, 14
			mov	ax, word ptr GrphPal[eax*2]
			mov	word ptr Grp_LineBufSP[edx], ax
			mov	word ptr Grp_LineBufSP2[edx], 0
		gp4osplineskip:
			add	esi, 2
			add	edx, 2
			dec	bx
			jnz	gp4ospline_cnt
			sub	esi, 0400h
		gp4ospline_cnt:
//			dec	cx
//			jnz	gp4osplinelp
			loop	gp4osplinelp
			pop	esi
		}
/*	}
	else
	{
		__asm
		{
			push	esi
			mov	esi, scry
			add	esi, VLINE
			and	esi, 511
			shl	esi, 10
			mov	ebx, scrx
			and	ebx, 511
			add	esi, ebx
			add	esi, ebx
//			add	esi, vram
			test	page, 2
			jz	gp4splinepage0
			inc	esi
		gp4splinepage0:
			xor	bx, 511
			inc	bx
			mov	ecx, TextDotX
			xor	edx, edx
		gp4splinelp:
			mov	al, byte ptr GVRAM[esi]
			test	al, 1
			jnz	gp4splinesp
			and	eax, 14
			jz	gp4splineskip_2
			mov	ax, word ptr GrphPal[eax*2]
			mov	Grp_LineBuf[edx], ax
		gp4splineskip_2:
			mov	word ptr Grp_LineBufSP[edx], 0
			jmp	gp4splineskip
		gp4splinesp:
			and	eax, 14
			jz	gp4splineskip_2
			mov	ax, word ptr GrphPal[eax*2]
//			mov	Grp_LineBuf[edx], ax
			mov	word ptr Grp_LineBufSP[edx], ax
		gp4splineskip:
			add	esi, 2
			add	edx, 2
			dec	bx
			jnz	gp4spline_cnt
			sub	esi, 0400h
		gp4spline_cnt:
//			dec	cx
//			jnz	gp4splinelp
			loop	gp4splinelp
			pop	esi
		}
	}
*/	}
}


void FASTCALL Grp_DrawLine4hSP(void)
{
	__asm
	{
		push	esi
		push	edi
		mov	esi, GrphScrollY[0]
		add	esi, VLINE
		mov	al, CRTC_Regs[0x29]
		and	al, 1ch
		cmp	al, 1ch
		jne	gp4hsplinenotspecial
		add	esi, VLINE
	gp4hsplinenotspecial:
		and	esi, 1023
		test	esi, 512
		jnz	gp4hsp_plane23
		shl	esi, 10
		mov	edx, GrphScrollX[0]
		mov	edi, edx
		and	edx, 511
		add	esi, edx
		add	esi, edx
		mov	cl, 00h
		test	edi, 512
		jz	gp4hsp_main
		add	cl, 4
		jmp	gp4hsp_main
	gp4hsp_plane23:
		and	esi, 511
		shl	esi, 10
		mov	edx, GrphScrollX[0]
		mov	edi, edx
		and	edx, 511
		add	esi, edx
		add	esi, edx
		mov	cl, 08h
		test	edi, 512
		jz	gp4hsp_main
		add	cl, 4
	gp4hsp_main:
		and	edi, 511
		xor	di, 511
		inc	di
		mov	ebx, TextDotX
		xor	edx, edx
	gp4hsplinelp:
		mov	ax, word ptr GVRAM[esi]
		shr	ax, cl
		test	ax, 1
		jnz	gp4hsplinesp
		and	eax, 14
		mov	ax, word ptr GrphPal[eax*2]
		mov	word ptr Grp_LineBufSP[edx], 0
		mov	Grp_LineBufSP2[edx], ax
		jmp	gp4hsplineskip
	gp4hsplinesp:
		and	eax, 14
		mov	ax, word ptr GrphPal[eax*2]
		mov	word ptr Grp_LineBufSP[edx], ax
		mov	word ptr Grp_LineBufSP2[edx], 0
	gp4hsplineskip:
		add	esi, 2
		add	edx, 2
		dec	di
		jnz	gp4hspline_cnt
		sub	esi, 0800h
	gp4hspline_cnt:
		dec	bx
		jnz	gp4hsplinelp
//		loop	gp4hsplinelp
		pop	edi
		pop	esi
	}
}



// -------------------------------------------------
// --- 半透明の対象となるページの描画 --------------
// 2ページ以上あるグラフィックモードのみなので、
// 256色2面 or 16色4面のモードのみ。
// 256色時は、Opaqueでない方のモードはいらないかも…
// （必ずOpaqueモードの筈）
// -------------------------------------------------
// ここはまだ32色x4面モードの実装をしてないれす…
// （れじすた足りないよぅ…）
// -------------------------------------------------
							// やけにすっきり
LABEL void FASTCALL Grp_DrawLine8TR(int page, int opaq) {

	__asm {
			or	edx, edx
			jz	opaq_zero
			push	ebx
			push	ecx
			push	esi
			push	edi
			and	ecx, 1
			mov	esi, GrphScrollY[ecx*8]
			add	esi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp8trlinenotspecial
			add	esi, VLINE
		gp8trlinenotspecial:
			and	esi, 511
			shl	esi, 10
			mov	ebx, GrphScrollX[ecx*8]
			and	ebx, 511
			add	esi, ecx
			mov	edx, TextDotX
			xor	edi, edi
		gp8otrlinelp:
			movzx	eax, word ptr Grp_LineBufSP[edi]
			movzx	ecx, byte ptr GVRAM[esi+ebx*2]
			or	ax, ax
			jnz	gp8otrlinetr
			mov	cx, word ptr GrphPal[ecx*2]
			jmp	gp8otrlinenorm
		gp8otrlinetr:
			jcxz	gp8otrlinenorm		// けろぴー…
			mov	cx, word ptr GrphPal[ecx*2]
			jcxz	gp8otrlinenorm		// けろぴー…
			and	ax, Pal_HalfMask
			test	cx, Ibit
			jz	gp8otrlinetrI
			or	ax, Pal_Ix2
		gp8otrlinetrI:
			and	cx, Pal_HalfMask
			add	cx, ax			// 17bit計算中
			rcr	cx, 1			// 17bit計算中
		gp8otrlinenorm:
			mov	Grp_LineBuf[edi], cx
			inc	bx
			and	bh, 1			// and	bx, 511
			add	edi, 2
			dec	dx
			jnz	gp8otrlinelp
			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
		opaq_zero:
			ret
	}
}

LABEL void FASTCALL Grp_DrawLine4TR(DWORD page, int opaq) {

	__asm {
			push	ebx
			push	ecx
			push	esi
			push	edi

			and	ecx, 3
			mov	esi, GrphScrollY[ecx*4]
			add	esi, VLINE
			mov	al, CRTC_Regs[0x29]
			and	al, 1ch
			cmp	al, 1ch
			jne	gp4trlinenotspecial
			add	esi, VLINE
		gp4trlinenotspecial:
			and	esi, 511
			shl	esi, 10
			mov	ebx, GrphScrollX[ecx*4]
			and	ebx, 511
			xor	edi, edi

			shr	cl, 1
			jnc	pagebit0eq0		// jmp (page 0 or 2)

			add	esi, ecx		// ecx = page/2
			or	edx, edx
			je	gp4trline2page0

			mov	edx, TextDotX
		gp4otrlinelp2:
			movzx	eax, word ptr Grp_LineBufSP[edi]
			movzx	ecx, byte ptr GVRAM[esi+ebx*2]
			shr	cl, 4
			or	ax, ax
			jnz	gp4otrlinetr2
			mov	cx, word ptr GrphPal[ecx*2]
			jmp	gp4otrlinenorm2
		gp4otrlinetr2:
			jcxz	gp4otrlinenorm2		// けろぴー
			mov	cx, word ptr GrphPal[ecx*2]
			jcxz	gp4otrlinenorm2		// けろぴー
			and	ax, Pal_HalfMask
			test	cx, Ibit
			jz	gp4otrlinetr2I
			or	ax, Pal_Ix2
		gp4otrlinetr2I:
			and	cx, Pal_HalfMask
			add	cx, ax			// 17bit計算中
			rcr	cx, 1			// 17bit計算中
		gp4otrlinenorm2:
			mov	Grp_LineBuf[edi], cx
			inc	bx
			and	bh, 1			// and	bx, 511
			add	edi, 2
			dec	dx
			jnz	gp4otrlinelp2
			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
			ret

		gp4trline2page0:
			mov	edx, TextDotX
		gp4trlinelp2:
			movzx	eax, word ptr Grp_LineBufSP[edi]
			or	ax, ax
			jnz	gp4trlinetr2
			movzx	ecx, byte ptr GVRAM[esi+ebx*2]
			shr	cl, 4
			jcxz	gp4trlineskip2
			mov	cx, word ptr GrphPal[ecx*2]
			jmp	gp4trlinenorm2
		gp4trlinetr2:
			movzx	ecx, byte ptr GVRAM[esi+ebx*2]
			shr	cl, 4
			jcxz	gp4trlinenorm2		// けろぴー
			mov	cx, word ptr GrphPal[ecx*2]
			jcxz	gp4trlineskip2		// けろぴー
			and	ax, Pal_HalfMask
			test	cx, Ibit
			jz	gp4trlinetr2I
			or	ax, Pal_Ix2
		gp4trlinetr2I:
			and	cx, Pal_HalfMask
			add	cx, ax			// 17bit計算中
			rcr	cx, 1			// 17bit計算中
		gp4trlinenorm2:
			mov	Grp_LineBuf[edi], cx
		gp4trlineskip2:
			inc	bx
			and	bh, 1			// and	bx, 511
			add	edi, 2
			dec	dx
			jnz	gp4trlinelp2
			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
			ret

		pagebit0eq0:
			add	esi, ecx		// ecx = page/2
			or	edx, edx
			je	gp4trlinepage0

			mov	edx, TextDotX
		gp4otrlinelp:
			mov	cl, byte ptr GVRAM[esi+ebx*2]
			and	ecx, 15
			movzx	eax, word ptr Grp_LineBufSP[edi]
			or	ax, ax
			jnz	gp4otrlinetr
			mov	cx, word ptr GrphPal[ecx*2]
			jmp	gp4otrlinenorm
		gp4otrlinetr:
			jcxz	gp4otrlinenorm		// けろぴー
			mov	cx, word ptr GrphPal[ecx*2]
			jcxz	gp4otrlinenorm		// けろぴー
			and	ax, Pal_HalfMask
			test	cx, Ibit
			jz	gp4otrlinetrI
			or	ax, Pal_Ix2
		gp4otrlinetrI:
			and	cx, Pal_HalfMask
			add	cx, ax			// 17bit計算中
			rcr	cx, 1			// 17bit計算中
		gp4otrlinenorm:
			mov	Grp_LineBuf[edi], cx
			inc	bx
			and	bh, 1			// and	bx, 511
			add	edi, 2
			dec	dx
			jnz	gp4otrlinelp
			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
			ret

		gp4trlinepage0:
			mov	edx, TextDotX
		gp4trlinelp:
			mov	cl, byte ptr GVRAM[esi+ebx*2]
			and	ecx, 15
			movzx	eax, word ptr Grp_LineBufSP[edi]
			or	ax, ax
			jnz	gp4trlinetr

			jcxz	gp4trlineskip
			mov	cx, word ptr GrphPal[ecx*2]
			jmp	gp4trlinenorm

		gp4trlinetr:
			jcxz	gp4trlinenorm		// けろぴー
			mov	cx, word ptr GrphPal[ecx*2]
			jcxz	gp4trlineskip		// けろぴー
			and	ax, Pal_HalfMask
			test	cx, Ibit
			jz	gp4trlinetrI
			or	ax, Pal_Ix2
		gp4trlinetrI:
			and	cx, Pal_HalfMask
			add	cx, ax			// 17bit計算中
			rcr	cx, 1			// 17bit計算中
		gp4trlinenorm:
			mov	Grp_LineBuf[edi], cx
		gp4trlineskip:
			inc	bx
			and	bh, 1			// and	bx, 511
			add	edi, 2
			dec	dx
			jnz	gp4trlinelp
			pop	edi
			pop	esi
			pop	ecx
			pop	ebx
			ret
	}
}


