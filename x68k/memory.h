#ifndef _winx68k_memory
#define _winx68k_memory

#include "common.h"

#define	Memory_ReadB		dma_readmem24
#define Memory_ReadW		dma_readmem24_word
#define Memory_ReadD		dma_readmem24_dword

#define	Memory_WriteB		dma_writemem24
#define Memory_WriteW		dma_writemem24_word
#define Memory_WriteD		dma_writemem24_dword

extern	BYTE*	IPL;
extern	BYTE*	MEM;
extern	BYTE*	OP_ROM;
extern	BYTE*	FONT;

extern	BYTE	BusErrFlag;
extern	DWORD	BusErrAdr;

void FASTCALL Memory_ErrTrace(void);
void FASTCALL Memory_IntErr(int i);

void Memory_Init(void);

BYTE FASTCALL dma_readmem24(DWORD adr);
WORD FASTCALL dma_readmem24_word(DWORD adr);
DWORD FASTCALL dma_readmem24_dword(DWORD adr);

void FASTCALL dma_writemem24(DWORD adr, BYTE data);
void FASTCALL dma_writemem24_word(DWORD adr, WORD data);
void FASTCALL dma_writemem24_dword(DWORD adr, DWORD data);

void FASTCALL cpu_setOPbase24(DWORD adr);

void FASTCALL Memory_SetSCSIMode(void);

#endif
