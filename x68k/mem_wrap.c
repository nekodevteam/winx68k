// ---------------------------------------------------------------------------------------
//  MEMRY.C - メモリ＆I/O管理
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	"irqh.h"
//#include	"opm.h"
#include	"m68000.h"
#include	"memory.h"
#include	"sram.h"
#include	"tvram.h"
#include	"gvram.h"

#include	"mfp.h"
#include	"fdc.h"
#include	"dmac.h"
#include	"ioc.h"
#include	"rtc.h"
#include	"adpcm.h"
#include	"sasi.h"
#include	"bg.h"
#include	"palette.h"
#include	"crtc.h"
#include	"pia.h"
#include	"scc.h"
#include	"midi.h"

#include	"../fmgen/fmg_wrap.h"

extern	BYTE*	MEM;
extern	BYTE*	IPL;
extern	BYTE*	OP_ROM;
extern	BYTE*	FONT;
extern	BYTE	traceflag;

void FASTCALL Memory_ErrTrace(void)
{
#ifdef WIN68DEBUG
	FILE *fp;
	fp=fopen("_buserr.txt", "a");
	if (BusErrFlag==3)
		fprintf(fp, "BusErr - SetOP to $%08X  @ $%08X\n", BusErrAdr, regs.pc);
	else if (BusErrFlag==2)
		fprintf(fp, "BusErr - Write to $%08X  @ $%08X\n", BusErrAdr, regs.pc);
	else
		fprintf(fp, "BusErr - Read from $%08X  @ $%08X\n", BusErrAdr, regs.pc);
	fclose(fp);
//	traceflag ++;
//	m68000_ICount = 0;
#endif
}


void FASTCALL Memory_IntErr(int i)
{
#ifdef WIN68DEBUG
	FILE *fp;
	fp=fopen("_interr.txt", "a");
	fprintf(fp, "IntErr - Int.No%d  @ $%08X\n", i, regs.pc);
	fclose(fp);
#else
	(void)i;
#endif
}


void Memory_Init(void)
{
	cpu_setOPbase24((DWORD)regs.pc);
}


// ---------------------- メモリアクセス部 → MEMORY.ASM に移動 -----------------------
#if 0
BYTE FASTCALL Memory_ReadB(DWORD adr)
{
	DWORD ah;
	adr &= 0xffffff;
	ah = adr>>12;

	if (adr<0xa00000)
		return MEM[adr^1];
	else if (adr>=0xfe0000)
		return IPL[(adr^1)-0xfe0000];
	else if ((adr>=0xf00000)&&(adr<0xfc0000))
		return FONT[adr-0xf00000];
	else if (ah==0xe88)
		return MFP_Read(adr);
	else if (ah==0xed0)
		return SRAM_Read(adr);
	else if (ah==0xe94)
		return FDC_Read(adr);
	else if (ah==0xe84)
		return DMA_Read(adr);
	else if (ah==0xe9c)
		return IOC_Read(adr);
	else if (ah==0xe8a)
		return RTC_Read(adr);
	else if (ah==0xe92)
		return ADPCM_Read(adr);
	else if ((adr>=0xe96001)&&(adr<0xe96008))
		return SASI_Read(adr);
	else if ((adr>=0xe00000)&&(adr<0xe80000))
		return TVRAM_Read(adr);
	else if ((adr>=0xc00000)&&(adr<0xe00000))
		return GVRAM_Read(adr);
	else if ((adr>=0xeb0000)&&(adr<0xec0000))
		return BG_Read(adr);
	else if ((adr>=0xe82000)&&(adr<0xe82400))
		return Pal_Read(adr);
	else if ((adr>=0xe82400)&&(adr<0xe82602))
		return VCtrl_Read(adr);
	else if ((adr>=0xe80000)&&(adr<0xe80482))
		return CRTC_Read(adr);
	else if ((adr>=0xe9a000)&&(adr<0xe9a008))
		return PIA_Read(adr);
	else if ((adr>=0xe98000)&&(adr<0xe98008))
		return SCC_Read(adr);
	else if ((adr>=0xeafa01)&&(adr<0xeafa0f))
		return MIDI_Read(adr);
	else if (adr==0xe90003)		// FM音源ST
		return OPM_Read(0);
	else
		return 0xff;
}


WORD FASTCALL Memory_ReadW(DWORD adr)
{
	return ((Memory_ReadB(adr)<<8)|(Memory_ReadB(adr+1)));
}

DWORD FASTCALL Memory_ReadD(DWORD adr)
{
	return ((Memory_ReadB(adr)<<24)|(Memory_ReadB(adr+1)<<16)|(Memory_ReadB(adr+2)<<8)|(Memory_ReadB(adr+3)));
}

void FASTCALL Memory_WriteB(DWORD adr, BYTE data)
{
	DWORD ah;
	adr &= 0xffffff;
	ah = adr>>12;

	if (adr<0xa00000)
		MEM[adr^1] = data;
	else if (ah==0xe88)
		MFP_Write(adr, data);
	else if (ah==0xed0)
		SRAM_Write(adr, data);
	else if (ah==0xe94)
		FDC_Write(adr, data);
	else if (ah==0xe84)
		DMA_Write(adr, data);
	else if (ah==0xe9c)
		IOC_Write(adr, data);
	else if (ah==0xe92)
		ADPCM_Write(adr, data);
	else if ((adr>=0xe82000)&&(adr<0xe82400))
		Pal_Write(adr, data);
	else if ((adr>=0xe82400)&&(adr<0xe82602))
		VCtrl_Write(adr, data);
	else if ((adr>=0xe80000)&&(adr<0xe80482))
		CRTC_Write(adr, data);
	else if ((adr>=0xe9a000)&&(adr<0xe9a008))
		PIA_Write(adr, data);
	else if ((adr>=0xe98000)&&(adr<0xe98008))
		SCC_Write(adr, data);
	else if ((adr>=0xeafa01)&&(adr<0xeafa0f))
		MIDI_Write(adr, data);
	else if (adr==0xe90001)		// FM音源Reg
		OPM_Write(0, data);
	else if (adr==0xe90003)		// FM音源Data
		OPM_Write(1, data);
	else if ((adr>=0xe96001)&&(adr<0xe96008))
		SASI_Write(adr, data);
	else if ((adr>=0xe00000)&&(adr<0xe80000))
		TVRAM_Write(adr, data);
	else if ((adr>=0xeb0000)&&(adr<0xec0000))
		BG_Write(adr, data);
	else if ((adr>=0xc00000)&&(adr<0xe00000))
		GVRAM_Write(adr, data);
}

void FASTCALL Memory_WriteW(DWORD adr, WORD data)
{
	Memory_WriteB(adr  , (BYTE)((data>>8)&0xff));
	Memory_WriteB(adr+1, (BYTE)((data   )&0xff));
}

void FASTCALL Memory_WriteD(DWORD adr, DWORD data)
{
	Memory_WriteB(adr  , (BYTE)((data>>24)&0xff));
	Memory_WriteB(adr+1, (BYTE)((data>>16)&0xff));
	Memory_WriteB(adr+2, (BYTE)((data>>8 )&0xff));
	Memory_WriteB(adr+3, (BYTE)((data    )&0xff));
}

void FASTCALL cpu_setOPbase24(DWORD adr)
{
	int no=0;
	if (adr<0xc00000)
	{
		OP_ROM = MEM;
//		no = 1;
	}
	else if ((adr>=0xc00000)&&(adr<0xe00000))
	{
		OP_ROM = GVRAM-0xc00000;
//		no = 2;
	}
	else if ((adr>=0xe00000)&&(adr<0xe80000))
	{
		OP_ROM = TVRAM-0xe00000;
//		no = 3;
	}
	else if ((adr>=0xed0000)&&(adr<0xed4000))
	{
		OP_ROM = SRAM-0xed0000;
//		no = 4;
	}
	else if (adr>=0xfe0000)
	{
		OP_ROM = IPL-0xfe0000;
//		no = 5;
	}
	else
	{
		char buf[50];
		sprintf(buf, "Un-assigned Bank : %08Xh", adr);
		Error(buf);
	}
/*{
FILE *fp;
fp=fopen("0mem.txt", "a");
fprintf(fp, "Set Base : %08X -> %08X (%d)\n", adr, OP_ROM, no);
fclose(fp);
}*/
}
#endif