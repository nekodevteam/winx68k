#ifndef _winx68k_winui_h
#define _winx68k_winui_h

extern	BYTE	Debug_Text, Debug_Grp, Debug_Sp;
extern	DWORD	LastClock[4];

void WinUI_Init(void);
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

#endif //winx68k_winui_h
