// MOUSE.C - マウス入力

#include "common.h"
#include "winx68k.h"
#include "windraw.h"
#include "resource.h"
#include "prop.h"
#include "../x68k/scc.h"
#include "../x68k/crtc.h"

int	MousePosX = 0;
int	MousePosY = 0;
BYTE	MouseStat = 0;
BYTE	MouseSW = 0;

POINT	CursorPos;
int	mousex=0, mousey=0;

void Mouse_Init(void)
{
	MousePosX = (TextDotX / 2);
	MousePosY = (TextDotY / 2);
	MouseStat = 0;
}


// ----------------------------------
//	Mouse Event Occured
// ----------------------------------
void Mouse_Event(DWORD wparam, DWORD lparam)
{
	if (MouseSW)
	{
		MousePosX = LOWORD(lparam);
		MousePosY = HIWORD(lparam);
		if (wparam&MK_LBUTTON) MouseStat|=1; else MouseStat&=0xfe;
		if (wparam&MK_RBUTTON) MouseStat|=2; else MouseStat&=0xfd;
	}
}


// ----------------------------------
//	Mouse Data send to SCC
// ----------------------------------
void Mouse_SetData(void)
{
	POINT pt;
	int x=0, y=0;

	if (MouseSW)
	{
		//mousex += (MousePosX-(SCREEN_WIDTH/2))*Config.MouseSpeed;
		//mousey += (MousePosY-(SCREEN_HEIGHT/2))*Config.MouseSpeed;
		mousex += (MousePosX-(TextDotX/2))*Config.MouseSpeed;
		mousey += (MousePosY-(TextDotY/2))*Config.MouseSpeed;
		x = mousex/10;
		y = mousey/10;
		mousex -= x*10;
		mousey -= y*10;
		MouseSt = MouseStat;

		if (x>127)
		{
			MouseSt |= 0x10;
			MouseX = 127;
		}
		else if (x<-128)
		{
			MouseSt |= 0x20;
			MouseX = -128;
		}
		else
			MouseX = (signed char)x;
		if (y>127)
		{
			MouseSt |= 0x40;
			MouseY = 127;
		}
		else if (y<-128)
		{
			MouseSt |= 0x80;
			MouseY = -128;
		}
		else
			MouseY = (signed char)y;

		//MousePosX = (SCREEN_WIDTH / 2);
		//MousePosY = (SCREEN_HEIGHT / 2);
		//pt.x = (SCREEN_WIDTH / 2);
		//pt.y = (SCREEN_HEIGHT / 2);
		MousePosX = (TextDotX / 2);
		MousePosY = (TextDotY / 2);
		pt.x = (TextDotX / 2);
		pt.y = (TextDotY / 2);
		ClientToScreen(hWndMain, &pt);
		SetCursorPos(pt.x, pt.y);
	}
	else
	{
		MouseSt = 0;
		MouseX = 0;
		MouseY = 0;
	}
}


// ----------------------------------
//	Start Capture
// ----------------------------------
void Mouse_StartCapture(int flag)
{
	RECT rect;
	POINT pt;
	//HMENU	hmenu = GetMenu(hWndMain);

	if ((flag)&&(!MouseSW))
	{
		GetCursorPos(&CursorPos);

		pt.x = 0;
		pt.y = 0;
		ClientToScreen(hWndMain, &pt);
		//SetRect(&rect, pt.x, pt.y, pt.x + SCREEN_WIDTH, pt.y + SCREEN_HEIGHT);
		SetRect(&rect, pt.x, pt.y, pt.x + TextDotX, pt.y + TextDotY);
		ClipCursor(&rect);

		if (!FullScreenFlag)
			SetCapture(hWndMain);
		ShowCursor(FALSE);
		//pt.x = (SCREEN_WIDTH / 2);
		//pt.y = (SCREEN_HEIGHT / 2);
		pt.x = (TextDotX / 2);
		pt.y = (TextDotY / 2);
		ClientToScreen(hWndMain, &pt);
		SetCursorPos(pt.x, pt.y);

		MouseSW = 1;
	}
	if ((!flag)&&(MouseSW))
	{
		ReleaseCapture();
		ShowCursor(TRUE);
		ClipCursor(NULL);
		MouseSW = 0;
		SetCursorPos(CursorPos.x, CursorPos.y);
	}
}


void Mouse_ChangePos(void)
{
	RECT rect;
	POINT pt;
	if (MouseSW)
	{
		pt.x = 0;
		pt.y = 0;
		ClientToScreen(hWndMain, &pt);
		//SetRect(&rect, pt.x, pt.y, pt.x + SCREEN_WIDTH, pt.y + SCREEN_HEIGHT);
		SetRect(&rect, pt.x, pt.y, pt.x + TextDotX, pt.y + TextDotY);
		ClipCursor(&rect);

		pt.x = (TextDotX / 2);
		pt.y = (TextDotY / 2);
		ClientToScreen(hWndMain, &pt);
		SetCursorPos(pt.x, pt.y);
	}
}
