#ifndef winx68k_fileio_h
#define winx68k_fileio_h

#include "common.h"

#define	FILEH		HANDLE

#define	FSEEK_SET	0
#define	FSEEK_CUR	1
#define	FSEEK_END	2

char *getFileName(char *filename);

FILEH	File_Open(BYTE *filename);
FILEH	File_Create(BYTE *filename);
DWORD	File_Seek(FILEH handle, long pointer, short mode);
DWORD	File_Read(FILEH handle, void *data, DWORD length);
DWORD	File_Write(FILEH handle, void *data, DWORD length);
short	File_Close(FILEH handle);
short	File_Attr(BYTE *filename);

void	File_SetCurDir(BYTE *exename);
FILEH	File_OpenCurDir(BYTE *filename);
FILEH	File_CreateCurDir(BYTE *filename);
short	File_AttrCurDir(BYTE *filename);

#endif
