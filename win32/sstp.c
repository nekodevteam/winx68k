// ---------------------------------------------------------------------------------------
//  SSTP message (偽春菜) routine for Keropi
//    ちょっと流行りに乗ってみただけ :-p
//    そして構造はでんぢゃー（マテ
//  ToDo : メッセージに暗号化掛けたいね ^^;;;
// ---------------------------------------------------------------------------------------

#include "common.h"
#include "sstp.h"
#include "prop.h"
#include "version.h"
#include "windraw.h"

#include <winsock.h>

static int SSTPFlag = 0;

// WinSockを準備するよ〜
void SSTP_Init(void)
{
	WSADATA wsd;
	int err;

	SSTPFlag = 0;

	err = WSAStartup(0x0202, &wsd);		// 0x0101でもい〜よ〜な気もする
	if ( err ) return;

	SSTPFlag = 1;
}


// WinSock撤収〜
void SSTP_Cleanup(void)
{
	if (SSTPFlag) {
		WSACleanup();
		SSTPFlag = 0;
	}
}


// 偽春菜に送るよ〜
void SSTP_SendPacket(char* buf)
{
	SOCKET s;
	struct sockaddr_in adr;
	struct hostent *hp;
	int err, len = 0;

	if (!SSTPFlag) return;

	len = strlen(buf);

	// そけっと準備
	s = socket(AF_INET, SOCK_STREAM, 0/*IPPROTO_TCP*/);

	if ( s!=INVALID_SOCKET ) {
		hp = gethostbyname("localhost");	// 名前解決できない場合を考えると、127.0.0.1の方が良いのかな？
		ZeroMemory(&adr, sizeof(struct sockaddr_in));
		memcpy(&(adr.sin_addr), hp->h_addr, hp->h_length);
		adr.sin_family = hp->h_addrtype;
		adr.sin_port = htons((unsigned short)Config.SSTP_Port);
		// 接続〜
		err = connect(s, (struct sockaddr*)&adr, sizeof(adr));
		if ( err!=SOCKET_ERROR ) {
			// 送信〜
			err = send(s, buf, len, 0);
		}

		// 送ったら返答も待たずに撤収〜（ぉぃ
		closesocket(s);
	}
}


// 偽春菜にメッセージを送るよ〜
void SSTP_Send(char* str)
{
	char buf[1024];
	sprintf(buf, "SEND SSTP/1.0\r\nSender: Keropi Ver.%s\r\nScript: %s\\e\r\nOption: nodescript,notranslate\r\n\r\n",
			APP_VER_STRING, str);
	SSTP_SendPacket(buf);
}


// 偽春菜とこみゅにけーしょん〜
void SSTP_Communicate(char* str)
{
	char buf[1024];
	sprintf(buf, "COMMUNICATE SSTP/1.1\r\nSender: Keropi Ver.%s\r\nSentence: %s\r\n\r\n",
			APP_VER_STRING, str);
	SSTP_SendPacket(buf);
}


// めっせ〜じあげる〜
void SSTP_GiveDoc(char* str)
{
	char buf[1024];
	sprintf(buf, "GIVE SSTP/1.1\r\nSender: Keropi Ver.%s\r\nDocument: %s\r\n\r\n",
			APP_VER_STRING, str);
	SSTP_SendPacket(buf);
}


// 曲あげる〜
void SSTP_GiveSong(char* str)
{
	char buf[1024];
	sprintf(buf, "GIVE SSTP/1.1\r\nSender: Keropi Ver.%s\r\nSongname: %s\r\n\r\n",
			APP_VER_STRING, str);
	SSTP_SendPacket(buf);
}


// なんかやれ〜（けろぴが受信をしないので意味ナシ）
void SSTP_Execute(char* str)
{
	char buf[1024];
	sprintf(buf, "EXECUTE SSTP/1.0\r\nSender: Keropi Ver.%s\r\nCommand: %s\r\n\r\n",
			APP_VER_STRING, str);
	SSTP_SendPacket(buf);
}


#include "sstpmes.inc"

// ↑のファイルのテキストは暗号化されてるので解読〜。
// って、ここ見ればすぐ分かっちゃうけどね（笑）。
void SSTP_Decode(char* mes, unsigned char* org)
{
	int i;
	for (i=0; *org; i++) {
		*mes++ = (char)((*org++)^0xfa);
	}
	*mes = 0;
}


// IDに従ってメッセージ送信
void SSTP_SendMes(int id)
{
	char mes[512];
	char buf[512];

	if ( !Config.SSTP_Enable ) return;
	if ( FullScreenFlag ) return;

	switch(id) {
		// けろぴ単体系のメッセージ
		case SSTPMES_OPEN:
			SSTP_Decode(mes, OPENINGMES[rand()%MAXOPENMES]);
			break;
		case SSTPMES_ABOUT:
			SSTP_Decode(buf, ABOUTMES[rand()%MAXABOUTMES]);
			sprintf(mes, buf, APP_VER_STRING);
			break;
		case SSTPMES_ERROR:
			SSTP_Decode(mes, ERRMES);
			break;
		case SSTPMES_EJECT0:
		case SSTPMES_EJECT1:
			SSTP_Decode(buf, EJCMES);
			sprintf(mes, buf, id-SSTPMES_EJECT0);
			break;
		case SSTPMES_MOUSEHELP:
			SSTP_Decode(mes, MSHMES);
			break;
		case SSTPMES_FULLSCRHELP:
			SSTP_Decode(mes, FSHMES);
			break;
		case SSTPMES_RESET:
			// 未実装
			return;
		case SSTPMES_NMI:
			SSTP_Decode(mes, NMIMES);
			break;
		case SSTPMES_AUTOSTRETCH:
			SSTP_Decode(mes, ASTMES);
			break;
		case SSTPMES_NONSTRETCH:
			SSTP_Decode(mes, NSTMES);
			break;
		case SSTPMES_FIXEDSTRETCH:
			SSTP_Decode(mes, FSTMES);
			break;
		case SSTPMES_X68STRETCH:
			SSTP_Decode(mes, XSTMES);
			break;
		case SSTPMES_DROPFD0:
		case SSTPMES_DROPFD1:
			SSTP_Decode(buf, DFDMES);
			sprintf(mes, buf, id-SSTPMES_DROPFD0);
			break;
		case SSTPMES_MAKEFONT:
			SSTP_Decode(mes, FNTMES);
			break;
		case SSTPMES_WAITFD:
			SSTP_Decode(mes, WFDMES);
			break;
		case SSTPMES_DUALBOOT:
			SSTP_Decode(mes, DLBMES);
			break;

		// 複合起動系メッセージ（多いなぁ）
		case SSTPMES_NP2:
			SSTP_Decode(mes, NP2MES);
			break;
		case SSTPMES_EX68:
			SSTP_Decode(mes, E68MES);
			break;
		case SSTPMES_XMIL:
			SSTP_Decode(mes, XMLMES);
			break;
		case SSTPMES_WINX1:
			SSTP_Decode(mes, WX1MES);
			break;
		case SSTPMES_XM7:
			SSTP_Decode(mes, XM7MES);
			break;
		case SSTPMES_MZ700WIN:
			SSTP_Decode(mes, M7WMES);
			break;
		case SSTPMES_IP6:
			SSTP_Decode(mes, IP6MES);
			break;
		case SSTPMES_ANEX86:
			SSTP_Decode(mes, A86MES);
			break;
		case SSTPMES_T98:
			SSTP_Decode(mes, T98MES);
			break;
		case SSTPMES_T98NEXT:
			SSTP_Decode(mes, T9NMES);
			break;
		case SSTPMES_XXX:
			SSTP_Decode(mes, XXXMES);
			break;
		case SSTPMES_TOOMANY:
			SSTP_Decode(mes, MNYMES);
			break;
		default:
			return;
	}
	SSTP_Send(mes);
}
