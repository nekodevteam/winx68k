// -----------------------------------------------------------------------
//   こまんどらいん処理なの。てきとーなの（ぉぃ。
// -----------------------------------------------------------------------
//
// えっとね、ここ見てる人はあんまりいないだろうけど、ここで説明しておくの（ぉ。
//
// ディスク挿入起動は、「Aファイル名」「A:ファイル名」「A=ファイル名」が通ります。
// ドライブ名は A/B もしくは 0/1 が通ります。また、ドライブ名の前に「-」か「/」が
// あっても可。
//
// 「NOCD」を記述する事で、ASPIによるCDチェックを飛ばしての起動が可能。この状態で
// 一回終了すれば、CD無し設定がINIに書き込まれるので、以降はこのオプションはいら
// ないはず。これも「-」「/」が前についててもいいです。


#include "common.h"
#include "winui.h"
#include "prop.h"
#include "../x68k/fdd.h"

#include <string.h>

static char fd0[MAX_PATH];
static char fd1[MAX_PATH];
char macrofile[MAX_PATH];

void SetCmdLineFD(void) {
	if ( fd0[0] ) FDD_SetFD(0, fd0, 0);
	if ( fd1[0] ) FDD_SetFD(1, fd1, 0);
}


void CheckCmdLineSub(char* s)
{
	if ( (*s=='-')||(*s=='/') ) s++;
	if ( (*s=='a')||(*s=='A')||(*s=='0') ) {
		if ( (*s=='=')||(*s==':') ) s++;
		strcpy(fd0, s+1);
	} else if ( (*s=='b')||(*s=='B')||(*s=='1') ) {
		if ( (*s=='=')||(*s==':') ) s++;
		strcpy(fd1, s+1);
	} else if ( (*s=='m')||(*s=='M') ) {
		if ( (*s=='=')||(*s==':') ) s++;
		strcpy(macrofile, s+1);
	} else {
		if ( (*s=='-')||(*s=='/') ) s++;
		_strupr(s);
		if ( !strcmp(s, "NOCD") ) {
			Config.CDROM_Enable = 0;
		}
	}
}


void CheckCmdLine(LPSTR cmd)
{
	int len;
	char buf[256];
	char* p;
	char* s = cmd;

	ZeroMemory(macrofile, MAX_PATH);
	len = strlen(s);
	while( (*s)&&((s-cmd)<len) ) {
		while( *s==0x20 ) s++;
		p = buf;
		while( (*s!=0x20)&&(*s) ) {
			*p++ = *s++;
		}
		*p = 0;
		if ( p!=buf ) CheckCmdLineSub(buf);
	}
}
