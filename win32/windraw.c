// ---------------------------------------------------------------------------------------
//  WINDRAW.C - DirectDrawによる画面描画
//              ビデオコントローラによるプライオリティ処理も含んでいます
// ---------------------------------------------------------------------------------------

#include	"common.h"
#include	<ddraw.h>
#include	"resource.h"
#include	"windraw.h"
#include	"winui.h"
#include	"status.h"
#include	"prop.h"
#include	"winx68k.h"
#include	"mouse.h"
#include	"../x68k/palette.h"
#include	"../x68k/tvram.h"
#include	"../x68k/gvram.h"
#include	"../x68k/bg.h"
#include	"../x68k/crtc.h"

#define SUPPORT_32BPP

#define BMPSIZEX 64
#define BMPSIZEY 64
#define BMPID IDB_KEROPI

	WORD	ScrBuf[FULLSCREEN_WIDTH*FULLSCREEN_HEIGHT];
//	WORD*	ScrBuf = 0;

	int	FullScreenFlag = 0;
	BYTE	Draw_DrawFlag = 1;

static	LPDIRECTDRAW4		dd = NULL;
static	LPDIRECTDRAWSURFACE4	prsurf = NULL;
static	LPDIRECTDRAWSURFACE4	bksurf = NULL;
static	LPDIRECTDRAWCLIPPER	clipper = NULL;
static	LPDIRECTDRAWSURFACE4	splashsurf = NULL;

	int	winx=0, winy=0;
	int	winh=0, winw=0;
	WORD	FrameCount = 0;
	int	SplashFlag = 0;

	WORD	WinDraw_Pal16B, WinDraw_Pal16R, WinDraw_Pal16G;

	int	WindowX = 0;
	int	WindowY = 0;


void WinDraw_InitWindowSize(WORD width, WORD height)
{
	RECT	rectWindow, rectClient;
	int	scx, scy;

	GetWindowRect(hWndMain, &rectWindow);
	GetClientRect(hWndMain, &rectClient);
	winw = width + (rectWindow.right - rectWindow.left)
					- (rectClient.right - rectClient.left);
	winh = height + (rectWindow.bottom - rectWindow.top)
					- (rectClient.bottom - rectClient.top);

	scx = GetSystemMetrics(SM_CXSCREEN);
	scy = GetSystemMetrics(SM_CYSCREEN);

	if (scx < winw)
		winx = (scx - winw) / 2;
	else if (winx < 0)
		winx = 0;
	else if ((winx + winw) > scx)
		winx = scx - winw;
	if (scy < winh)
		winy = (scy - winh) / 2;
	else if (winy < 0)
		winy = 0;
	else if ((winy + winh) > scy)
		winy = scy - winh;

	if (hWndStat) winh += heightStat;
}


void WinDraw_ChangeSize(void)
{
	DDBLTFX	ddbf;
	RECT	rct = {0, 0, 800, 600};
	int oldx=WindowX, oldy=WindowY, dif;
	Mouse_ChangePos();
	switch(Config.WinStrech)
	{
	case 0:		// No Stretch
		WindowX = TextDotX;
		WindowY = TextDotY;
		break;
	case 1:		// Fixed Stretch
		WindowX = 768;
		WindowY = 512;
		break;
	case 2:		// Auto Stretch
		if (TextDotX<=384) WindowX=TextDotX*2; else WindowX=TextDotX;
		if (TextDotY<=256) WindowY=TextDotY*2; else WindowY=TextDotY;
		break;
	case 3:		// Auto X68k Size
		if (TextDotX<=384) WindowX=TextDotX*2; else WindowX=TextDotX;
		if (TextDotY<=256) WindowY=TextDotY*2; else WindowY=TextDotY;
		dif = WindowX-WindowY;
		if ((dif>(-32))&&(dif<32)) WindowX = (int)(WindowX*1.25);	// 正方形に近い画面なら、としておこう
		break;
	}
	if ((WindowX>768)||(WindowX<=0)) {
		if (oldx)
			WindowX=oldx;
		else
			WindowX = oldx = 768;
	}
	if ((WindowY>512)||(WindowY<=0)) {
		if (oldy)
			WindowY=oldy;
		else
			WindowY = oldy = 512;
	}

	if ((oldx==WindowX)&&(oldy==WindowY)) return;

	if (FullScreenFlag)
	{
		ZeroMemory(&ddbf, sizeof(ddbf));
		ddbf.dwSize = sizeof(ddbf);
		ddbf.dwFillColor = 0;
		IDirectDrawSurface4_Blt(prsurf, &rct, NULL, NULL, DDBLT_COLORFILL, &ddbf);
		StatBar_Show(Config.FullScrFDDStat);
	}
	else
	{
		WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
		MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);
		WinDraw_InitWindowSize((WORD)WindowX, (WORD)WindowY);
		MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);
		StatBar_Show(Config.WindowFDDStat);
		Mouse_ChangePos();
	}
}

#ifdef SUPPORT_32BPP
static uint32_t* pPal15To32 = NULL;
BYTE ScrDirtyLine[1024];
#else	// SUPPORT_32BPP
static int dispflag = 0;
static DEVMODE oldDisp, newDisp;
#endif	// SUPPORT_32BPP

void WinDraw_StartupScreen(void)
{
	LPDIRECTDRAW	dd1;
	dd = NULL;

#ifndef SUPPORT_32BPP
	if ( EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &oldDisp) ) {
		if ( oldDisp.dmBitsPerPel!=16 ) {
			memcpy(&newDisp, &oldDisp, sizeof(DEVMODE));
			newDisp.dmBitsPerPel = 16;
			newDisp.dmFields = DM_BITSPERPEL|DM_DISPLAYFREQUENCY;
			if ( ChangeDisplaySettings(&newDisp, 0)==DISP_CHANGE_SUCCESSFUL ) {
				dispflag = 1;
			}
		}
	}
#endif	// !SUPPORT_32BPP

	if (DirectDrawCreate(NULL, &dd1, NULL) != DD_OK) {
		return/* FALSE*/;
	}
	// 環境によっては失敗する？ ので安全策〜
	if (IDirectDraw_QueryInterface(dd1, &IID_IDirectDraw4, (void **)&dd) != DD_OK) {
		dd = 0;
		return/* FALSE*/;
	}

	IDirectDraw_Release(dd1);
	dd1=0;
}


void WinDraw_CleanupScreen(void)
{
	if (dd     ) IDirectDraw4_Release(dd);
	dd=0;

#ifdef SUPPORT_32BPP
	if (pPal15To32)
		free(pPal15To32);
	pPal15To32 = NULL;
#else	// SUPPORT_32BPP
	if ( dispflag )
		ChangeDisplaySettings(&oldDisp, 0);
#endif	// SUPPORT_32BPP
}


void WinDraw_ChangeMode(int flag) {
	DWORD	winstyle, winstyleex;
	//HMENU	hMenu = GetMenu(hWndMain);

	FullScreenFlag = flag;
	winstyle = GetWindowLong(hWndMain, GWL_STYLE);
	winstyleex = GetWindowLong(hWndMain, GWL_EXSTYLE);
	if (flag)
	{
		RECT rect;
		GetWindowRect(hWndMain, &rect);
		winx = rect.left;
		winy = rect.top;

		winstyle = (winstyle | WS_POPUP)
						& (~(WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU));
		winstyleex |= WS_EX_TOPMOST;
		SetWindowLong(hWndMain, GWL_STYLE, winstyle);
		SetWindowLong(hWndMain, GWL_EXSTYLE, winstyleex);
		CheckMenuItem(hMenu, IDM_TOGGLEFULLSCREEN, MF_CHECKED);
		CheckMenuItem(hMenu, IDM_MOUSE, MF_CHECKED);
		EnableMenuItem(hMenu, IDM_MOUSE, MF_GRAYED);
	}
	else
	{
		winstyle = (winstyle | WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU)
						& ~WS_POPUP;
		winstyleex &= (~WS_EX_TOPMOST);
		SetWindowLong(hWndMain, GWL_STYLE, winstyle);
		SetWindowLong(hWndMain, GWL_EXSTYLE, winstyleex);

		MoveWindow(hWndMain, winx, winy, winw, winh, TRUE);

		CheckMenuItem(hMenu, IDM_TOGGLEFULLSCREEN, MF_UNCHECKED);
		CheckMenuItem(hMenu, IDM_MOUSE, MF_UNCHECKED);
		EnableMenuItem(hMenu, IDM_MOUSE, MF_ENABLED);
	}
}


static void WinDraw_ShowSplash(void)
{
	POINT	pt;
	RECT	rectDst;
	RECT	rectSrc;

	pt.x = (768-BMPSIZEX+16);
	pt.y = (512-BMPSIZEY+16);
#ifdef SUPPORT_32BPP
	pt.x -= 16;
	pt.y -= 16;
	ClientToScreen(hWndMain, &pt);
#endif	// SUPPORT_32BPP
	SetRect(&rectDst, pt.x, pt.y, pt.x + BMPSIZEX, pt.y + BMPSIZEY);
	SetRect(&rectSrc, 0, 0, BMPSIZEX, BMPSIZEY);
#ifdef SUPPORT_32BPP
	IDirectDrawSurface4_Blt(prsurf, &rectDst, splashsurf, &rectSrc, DDBLT_WAIT/*|DDBLT_KEYSRC*/, NULL);
#else	// SUPPORT_32BPP
	IDirectDrawSurface4_Blt(bksurf, &rectDst, splashsurf, &rectSrc, DDBLT_WAIT/*|DDBLT_KEYSRC*/, NULL);
#endif	// SUPPORT_32BPP
}


void WinDraw_HideSplash(void)
{
	DDBLTFX	ddbf;
	RECT	rct = {768-BMPSIZEX+16, 512-BMPSIZEY+16, 768+16, 512+16};

//	IDirectDrawSurface4_SetClipper(prsurf, 0);
	ZeroMemory(&ddbf, sizeof(ddbf));
	ddbf.dwSize = sizeof(ddbf);
	ddbf.dwFillColor = 0;
	IDirectDrawSurface4_Blt(bksurf, &rct, NULL, NULL, DDBLT_COLORFILL, &ddbf);
	Draw_DrawFlag = 1;
}


int WinDraw_Init(void)
{
	DDSURFACEDESC2	ddsd;
//	DDSURFACEDESC	ddsd00;
	DDPIXELFORMAT	ddpf;

	HDC hdcs, hdcd;
	HANDLE hbmp,hbmpold;
	const int caps[] = {
//		DDSCAPS_VIDEOMEMORY | DDSCAPS_LOCALVIDMEM | DDSCAPS_PRIMARYSURFACE,
		DDSCAPS_SYSTEMMEMORY | DDSCAPS_PRIMARYSURFACE | DDSCAPS_OWNDC,
		DDSCAPS_SYSTEMMEMORY | DDSCAPS_PRIMARYSURFACE,
		DDSCAPS_PRIMARYSURFACE,
		0
	};
	int i;

	prsurf = NULL;
	bksurf = NULL;
	clipper = NULL;

	WindowX = 768;
	WindowY = 512;

	if (!dd) return FALSE;			// これも安全策の一環…

	if (FullScreenFlag)
	{
		IDirectDraw4_SetCooperativeLevel(dd, hWndMain,
					DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWREBOOT);
		if (IDirectDraw4_SetDisplayMode(dd, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT,
								 16, 0, 0) != DD_OK) {
			return FALSE;
		}
		IDirectDraw4_CreateClipper(dd, 0, &clipper, NULL);
		IDirectDrawClipper_SetHWnd(clipper, 0, hWndMain);

		for (i=0; caps[i]; i++) {
			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_CAPS;
			ddsd.ddsCaps.dwCaps = caps[i];
			if ( IDirectDraw4_CreateSurface(dd, &ddsd, &prsurf, NULL)==DD_OK ) break;
		}
		if ( !caps[i] ) return FALSE;

		ZeroMemory(&ddpf, sizeof(ddpf));
		ddpf.dwSize = sizeof(DDPIXELFORMAT);
		if (DD_OK != IDirectDrawSurface4_GetPixelFormat(prsurf, &ddpf)) {
			return FALSE;
		}
		WinDraw_Pal16B = (WORD)ddpf.dwBBitMask;
		WinDraw_Pal16R = (WORD)ddpf.dwRBitMask;
		WinDraw_Pal16G = (WORD)ddpf.dwGBitMask;
		Pal_SetColor();

		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY;
		ddsd.dwWidth = FULLSCREEN_WIDTH;
		ddsd.dwHeight = FULLSCREEN_HEIGHT;
		if (IDirectDraw4_CreateSurface(dd, &ddsd, &bksurf, NULL) != DD_OK) {
			return FALSE;
		}

		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_LPSURFACE;
		ddsd.lpSurface = ScrBuf;
		if (DD_OK != IDirectDrawSurface4_SetSurfaceDesc(bksurf, &ddsd, 0)) return FALSE;
	}
	else
	{
		IDirectDraw4_SetCooperativeLevel(dd, hWndMain, DDSCL_NORMAL);
		// Primary Surface
#ifdef SUPPORT_32BPP
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
		if ( IDirectDraw4_CreateSurface(dd, &ddsd, &prsurf, NULL)!=DD_OK ) return FALSE;
#else	// SUPPORT_32BPP
		for (i=0; caps[i]; i++) {
			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_CAPS;
			ddsd.ddsCaps.dwCaps = caps[i];
			if ( IDirectDraw4_CreateSurface(dd, &ddsd, &prsurf, NULL)==DD_OK ) break;
		}
		if ( !caps[i] ) return FALSE;
#endif	// SUPPORT_32BPP

		IDirectDraw4_CreateClipper(dd, 0, &clipper, NULL);
		IDirectDrawClipper_SetHWnd(clipper, 0, hWndMain);
		IDirectDrawSurface4_SetClipper(prsurf, clipper);

		ZeroMemory(&ddpf, sizeof(ddpf));
		ddpf.dwSize = sizeof(DDPIXELFORMAT);
		if (DD_OK != IDirectDrawSurface4_GetPixelFormat(prsurf, &ddpf)) {
			return FALSE;
		}
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY;
//		ddsd.ddsCaps.dwCaps = DDSCAPS_SYSTEMMEMORY | DDSCAPS_PRIMARYSURFACE | DDSCAPS_OWNDC;
//		ddsd.ddsCaps.dwCaps = DDSCAPS_VIDEOMEMORY | DDSCAPS_NONLOCALVIDMEM;
		ddsd.dwWidth = FULLSCREEN_WIDTH;
		ddsd.dwHeight = FULLSCREEN_HEIGHT;

		if (IDirectDraw4_CreateSurface(dd, &ddsd, &bksurf, NULL) != DD_OK) {
			return FALSE;
		}
		if (!(ddpf.dwFlags&DDPF_RGB)) {
			return FALSE;
		}
		if (ddpf.dwRGBBitCount==16) {
			WinDraw_Pal16B = (WORD)ddpf.dwBBitMask;
			WinDraw_Pal16R = (WORD)ddpf.dwRBitMask;
			WinDraw_Pal16G = (WORD)ddpf.dwGBitMask;
			Pal_SetColor();

			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_LPSURFACE;
			ddsd.lpSurface = ScrBuf;
			if (DD_OK != IDirectDrawSurface4_SetSurfaceDesc(bksurf, &ddsd, 0)) {
				return FALSE;
			}
		}
#ifdef SUPPORT_32BPP
		else if (ddpf.dwRGBBitCount==32) {
			unsigned int i = 0;
			pPal15To32 = (uint32_t *)malloc(0x8000 * sizeof(uint32_t));
			for (i = 0; i < 0x8000; i++) {
				const uint8_t r5 = (i >> 10) & 0x1f;
				const uint8_t g5 = (i >>  5) & 0x1f;
				const uint8_t b5 = (i >>  0) & 0x1f;
				const uint8_t r8 = (r5 << 3) + (r5 >> 2);
				const uint8_t g8 = (g5 << 3) + (g5 >> 2);
				const uint8_t b8 = (b5 << 3) + (b5 >> 2);
				pPal15To32[i] = (r8 << 16) + (g8 << 8) + b8;
			}
			WinDraw_Pal16B = 0x001f;
			WinDraw_Pal16G = 0x03e0;
			WinDraw_Pal16R = 0x7c00;
			Pal_SetColor();
		}
#endif	// SUPPORT_32BPP
		else {
			return FALSE;
		}
/*
		if (IDirectDrawSurface4_Lock(bksurf, NULL, &ddsd, DDLOCK_WAIT, NULL) == DD_OK) {
			ScrBuf = ddsd.lpSurface;
		} else {
			return FALSE;
		}
*/
	}

							// けろぴーすぷらっしゅ用意
	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof(ddsd);
	ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
	ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
	ddsd.dwWidth = BMPSIZEX;
	ddsd.dwHeight = BMPSIZEY;
	if (IDirectDraw4_CreateSurface(dd, &ddsd, &splashsurf, NULL) != DD_OK) return FALSE;
							// BITMAPを読み込む
	hbmp=LoadBitmap(hInst,MAKEINTRESOURCE(BMPID));
							// サーフェイスへ転送
	hdcs=CreateCompatibleDC(NULL);
	hbmpold=SelectObject(hdcs, hbmp);
	IDirectDrawSurface4_GetDC(splashsurf, &hdcd);
	BitBlt(hdcd, 0, 0, BMPSIZEX, BMPSIZEY, hdcs, 0, 0,SRCCOPY);
	IDirectDrawSurface4_ReleaseDC(splashsurf, hdcd);
	SelectObject(hdcs, hbmpold);
	DeleteDC(hdcs);
	DeleteObject(hbmp);

	return TRUE;
}


void WinDraw_Cleanup(void) {

	if ((FullScreenFlag) && (dd)) {
		IDirectDraw4_SetCooperativeLevel(dd, hWndMain, DDSCL_NORMAL);
#ifndef SUPPORT_32BPP
		if ( dispflag )
			IDirectDraw4_SetDisplayMode(dd, newDisp.dmPelsWidth, newDisp.dmPelsHeight,
				 newDisp.dmBitsPerPel, newDisp.dmDisplayFrequency, 0);
//		IDirectDraw4_RestoreDisplayMode(dd);
#endif	// !SUPPORT_32BPP
	}
	if (splashsurf ) IDirectDrawSurface4_Release(splashsurf);
	if (bksurf ) IDirectDrawSurface4_Release(bksurf);
	if (clipper) IDirectDrawClipper_Release(clipper);
	if (prsurf ) IDirectDrawSurface4_Release(prsurf);
	prsurf=0;
	bksurf=0;
	splashsurf=0;
	clipper=0;
}


void WinDraw_Redraw(void)
{
	TVRAM_SetAllDirty();
}


void WinDraw_ShowMenu(int flag)
{
	//DDBLTFX	ddbf;
	//RECT	rct = {0, 0, FULLSCREEN_WIDTH, FULLSCREEN_POSY};

	if (!FullScreenFlag) return;
	if (flag)
	{
		IDirectDrawSurface4_SetClipper(prsurf, clipper);
		//SetMenu(hWndMain, hMenu);
		DrawMenuBar(hWndMain);
	}
	else
	{
		IDirectDrawSurface4_SetClipper(prsurf, 0);
		//ZeroMemory(&ddbf, sizeof(ddbf));
		//ddbf.dwSize = sizeof(ddbf);
		//ddbf.dwFillColor = 0;
		//IDirectDrawSurface4_Blt(prsurf, &rct, NULL, NULL, DDBLT_COLORFILL, &ddbf);
		//SetMenu(hWndMain, NULL);
		StatBar_Redraw();
	}
}


void FASTCALL WinDraw_Draw(void)
{
	POINT	pt;
	RECT	rectDst;
	RECT	rectSrc;
//	DDSURFACEDESC2	LockedSurface;
	int sx, sy;

	FrameCount++;

	if ((!prsurf)||(!bksurf)) return;

#ifdef SUPPORT_32BPP
	if (SplashFlag)
		WinDraw_ShowSplash();
#endif	// SUPPORT_32BPP

	if (!Draw_DrawFlag) return;
	Draw_DrawFlag = 0;

#ifndef SUPPORT_32BPP
	if (SplashFlag)
		WinDraw_ShowSplash();
#endif	// !SUPPORT_32BPP

	if (TextDotX>SCREEN_WIDTH)  sx = (TextDotX-SCREEN_WIDTH)/2;  else sx = 0;
	if (TextDotY>SCREEN_HEIGHT) sy = (TextDotY-SCREEN_HEIGHT)/2; else sy = 0;

	if (FullScreenFlag)
	{
		pt.x = (800-WindowX)/2;
		pt.y = (600-WindowY)/2;
		ClientToScreen(hWndMain, &pt);
		SetRect(&rectDst, pt.x, pt.y, pt.x+WindowX, pt.y+WindowY);
		SetRect(&rectSrc, 16+sx, 16+sy, TextDotX+16-sx, TextDotY+16-sy);
	}
	else
	{
		pt.x = 0;
		pt.y = 0;
		ClientToScreen(hWndMain, &pt);
		SetRect(&rectDst, pt.x, pt.y, pt.x + WindowX, pt.y + WindowY);
		SetRect(&rectSrc, 16+sx, 16+sy, TextDotX+16-sx, TextDotY+16-sy);
	}

#ifdef SUPPORT_32BPP
	if (pPal15To32) {
		HRESULT r;
		DDSURFACEDESC2 ddsd;
		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		r = IDirectDrawSurface4_Lock(bksurf, NULL, &ddsd, DDLOCK_WAIT, NULL);
		if (r == DDERR_SURFACELOST) {
			IDirectDrawSurface4_Restore(bksurf);
			r = IDirectDrawSurface4_Lock(bksurf, NULL, &ddsd, DDLOCK_WAIT, NULL);
		}
		if (r == DD_OK) {
			int x, y;
			uint16_t* p = ScrBuf + (rectSrc.top * FULLSCREEN_WIDTH);
			intptr_t q = (intptr_t)ddsd.lpSurface + (rectSrc.top * ddsd.lPitch);
			for (y = rectSrc.top; y < rectSrc.bottom; y++) {
				if (ScrDirtyLine[y - 16]) {
					ScrDirtyLine[y - 16] = 0;
					for (x = rectSrc.left; x < rectSrc.right; x++) {
						((uint32_t *)q)[x] = pPal15To32[p[x] & 0x7fff];
					}
				}
				p += FULLSCREEN_WIDTH;
				q += ddsd.lPitch;
			}
			IDirectDrawSurface4_Unlock(bksurf, NULL);
		}
	}
#endif	// SUPPORT_32BPP

	if (IDirectDrawSurface4_Blt(prsurf, &rectDst, bksurf, &rectSrc, DDBLT_WAIT, NULL)
		== DDERR_SURFACELOST)
	{
		IDirectDrawSurface4_Restore(prsurf);
		WinDraw_Redraw();
	}
}


INLINE void WinDraw_DrawGrpLine(int opaq)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
	__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			mov	edx, offset Grp_LineBuf
			mov	ecx, TextDotX
			shr	cx, 1
		drawgrplineolp:
			mov	eax, [edx]
			mov	[ebx], eax
			add	edx, 4
			add	ebx, 4
			loop	drawgrplineolp
		}
	}
	else
	{
	__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			mov	edx, offset Grp_LineBuf
			mov	ecx, TextDotX
		drawgrplinelp:
			mov	ax, [edx]
			or	ax, ax
			jz	drawgrplineskip
			mov	[ebx], ax
		drawgrplineskip:
			add	edx, 2
			add	ebx, 2
			loop	drawgrplinelp
		}
	}
}


INLINE void WinDraw_DrawGrpLineNonSP(int opaq)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
	__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			mov	edx, offset Grp_LineBufSP2
			mov	ecx, TextDotX
		drawgrpnslinelpo:
			mov	ax, [edx]
			mov	[ebx], ax
			add	edx, 2
			add	ebx, 2
			loop	drawgrpnslinelpo
		}
	}
	else
	{
	__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			mov	edx, offset Grp_LineBufSP2
			mov	ecx, TextDotX
		drawgrpnslinelp:
			mov	ax, [edx]
			or	ax, ax
			jz	drawgrpnslineskip
			mov	[ebx], ax
		drawgrpnslineskip:
			add	edx, 2
			add	ebx, 2
			loop	drawgrpnslinelp
		}
	}
}


INLINE void WinDraw_DrawTextLine(int opaq, int td)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawtextlineolp:
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
		//	test	byte ptr (Text_TrFlag+16)[edx], 1
		//	jnz	drawtextlineoskip
		//	mov	ax, TextPal[0]
		//drawtextlineoskip:
			mov	[ebx], ax
			inc	edx
			add	ebx, 2
			loop	drawtextlineolp
		}
	}
	else
	{
		if (td)
		{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawtextlinetdlp:
			test	byte ptr (Text_TrFlag+16)[edx], 1
			jz	drawtextlinetdskip
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawtextlinetdskip
//mov	[ebx], 0xffff
			mov	[ebx], ax
		drawtextlinetdskip:
			inc	edx
			add	ebx, 2
			loop	drawtextlinetdlp
		}
		}
		else
		{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawtextlinelp:
		//	test	byte ptr (Text_TrFlag+16)[edx], 3
		//	jnz	drawtextlinenonskip
		//	mov	ax, TextPal[0]
		//	jmp	drawtextlinenonskip2
		//drawtextlinenonskip:
		//	test	byte ptr (Text_TrFlag+16)[edx], 1
		//	jz	drawtextlineskip
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
		//drawtextlinenonskip2:
			or	ax, ax
			jz	drawtextlineskip
			mov	[ebx], ax
		drawtextlineskip:
			inc	edx
			add	ebx, 2
			loop	drawtextlinelp
		}
		}
	}
}


INLINE void WinDraw_DrawTextLineTR(int opaq)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			xor	edi, edi
			xor	eax, eax
			mov	ecx, TextDotX
		drawtexttrlineolp:
			mov	di, word ptr Grp_LineBufSP[edx*2]
			or	di, di
			jnz	drawtexttrlineotr

			test	byte ptr (Text_TrFlag+16)[edx], 1
			jz	drawtexttrlineopal0

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			jmp	drawtexttrlineonorm

		drawtexttrlineopal0:
			xor	ax, ax
			jmp	drawtexttrlineonorm
		drawtexttrlineotr:
						// ねこーねこー
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawtexttrlineotrI
			add	di, Pal_Ix2
		drawtexttrlineotrI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中
		drawtexttrlineonorm:
			mov	[ebx], ax
			inc	edx
			add	ebx, 2
			dec	cx
			jnz	drawtexttrlineolp
//			loop	drawtexttrlineolp
			pop	edi
		}
	}
	else
	{
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawtexttrlinelp:
			test	byte ptr (Text_TrFlag+16)[edx], 1
			jz	drawtexttrlineskip

			mov	di, word ptr Grp_LineBufSP[edx*2]
			or	di, di
			jnz	drawtexttrlinetr

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawtexttrlineskip
			jmp	drawtexttrlinenorm

		drawtexttrlinetr:
						// ねこーねこー
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawtexttrlineskip
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawtexttrlinetrI
			add	di, Pal_Ix2
		drawtexttrlinetrI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中
		drawtexttrlinenorm:
			mov	[ebx], ax
		drawtexttrlineskip:
			inc	edx
			add	ebx, 2
			dec	cx
			jnz	drawtexttrlinelp
//			loop	drawtexttrlinelp
			pop	edi
		}
	}
}


INLINE void WinDraw_DrawTextLineTR2(int opaq)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			xor	edi, edi
			xor	eax, eax
			mov	ecx, TextDotX
		drawtexttrline2olp:
			mov	di, word ptr Grp_LineBufSP[edx*2]
			or	di, di
			jnz	drawtexttrline2otr

			test	byte ptr (Text_TrFlag+16)[edx], 1
			jz	drawtexttrline2opal0

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			jmp	drawtexttrline2onorm

		drawtexttrline2opal0:
			xor	ax, ax
			jmp	drawtexttrline2onorm
		drawtexttrline2otr:
						// ねこーねこー
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawtexttrline2otrI
			add	di, Pal_Ix2
		drawtexttrline2otrI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中
		drawtexttrline2onorm:
			mov	[ebx], ax
			inc	edx
			add	ebx, 2
			dec	cx
			jnz	drawtexttrline2olp
//			loop	drawtexttrline2olp
			pop	edi
		}
	}
	else
	{
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawtexttrline2lp:
			test	byte ptr (Text_TrFlag+16)[edx], 1
			jz	drawtexttrline2skip

			mov	di, word ptr Grp_LineBufSP[edx*2]
			or	di, di
			jnz	drawtexttrline2tr

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawtexttrline2skip
			jmp	drawtexttrline2norm

		drawtexttrline2tr:
						// ねこーねこー
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawtexttrline2skip
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawtexttrline2trI
			add	di, Pal_Ix2
		drawtexttrline2trI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中
		drawtexttrline2norm:
			mov	[ebx], ax
		drawtexttrline2skip:
			inc	edx
			add	ebx, 2
			dec	cx
			jnz	drawtexttrline2lp
//			loop	drawtexttrline2lp
			pop	edi
		}
	}
}


INLINE void WinDraw_DrawBGLine(int opaq, int td)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawbglineolp:
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
		//	test	byte ptr (Text_TrFlag+16)[edx], 3
		//	jnz	drawbglineoskip
		//	mov	ax, TextPal[0]
		//drawbglineoskip:
			mov	[ebx], ax
			inc	edx
			add	ebx, 2
			loop	drawbglineolp
		}
	}
	else
	{
		if (td)
		{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawbglinetdlp:
			test	byte ptr (Text_TrFlag+16)[edx], 2
			jz	drawbglinetdskip
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawbglinetdskip
			mov	[ebx], ax
		drawbglinetdskip:
			inc	edx
			add	ebx, 2
			loop	drawbglinetdlp
		}
		}
		else
		{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawbglinelp:
		//	test	byte ptr (Text_TrFlag+16)[edx], 3
		//	jnz	drawbglinenonskip
		//	mov	ax, TextPal[0]
		//	jmp	drawbglinenonskip2
		//drawbglinenonskip:
		//	test	byte ptr (Text_TrFlag+16)[edx], 2
		//	jz	drawbglineskip
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
		//drawbglinenonskip2:
			or	ax, ax
			jz	drawbglineskip
			mov	[ebx], ax
		drawbglineskip:
			inc	edx
			add	ebx, 2
			loop	drawbglinelp
		}
		}
	}
}


INLINE void WinDraw_DrawBGLineTR(int opaq)
{
	DWORD adr = ((VLINE+16)*1600+32);
	if (opaq)
	{
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			xor	edi, edi
			xor	eax, eax
			mov	ecx, TextDotX
		drawbgtrlineolp:
			mov	di, word ptr Grp_LineBufSP[edx*2]
			or	di, di
			jnz	drawbgtrlineotr

		//	test	byte ptr (Text_TrFlag+16)[edx], 2
		//	jz	drawbgtrlineopal0

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			jmp	drawbgtrlineonorm

		//drawbgtrlineopal0:
		//	xor	ax, ax
		//	jmp	drawbgtrlineonorm

		drawbgtrlineotr:

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawbgtrlineotrI
			add	di, Pal_Ix2
		drawbgtrlineotrI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中

		drawbgtrlineonorm:
			mov	[ebx], ax
			inc	edx
			add	ebx, 2
			dec	cx
			jnz	drawbgtrlineolp
//			loop	drawbgtrlineolp
			pop	edi
		}
	}
	else
	{
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			mov	ecx, TextDotX
		drawbgtrlinelp:
			test	byte ptr (Text_TrFlag+16)[edx], 2
			jz	drawbgtrlineskip

			mov	di, word ptr Grp_LineBufSP[edx*2]
			or	di, di
			jnz	drawbgtrlinetr

			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawbgtrlineskip
			jmp	drawbgtrlinenorm

		drawbgtrlinetr:
						// ねこーねこー
			mov	ax, word ptr (BG_LineBuf+32)[edx*2]
			or	ax, ax
			jz	drawbgtrlineskip
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawbgtrlinetrI
			add	di, Pal_Ix2
		drawbgtrlinetrI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中

		drawbgtrlinenorm:
			mov	[ebx], ax
		drawbgtrlineskip:
			inc	edx
			add	ebx, 2
			dec	cx
			jnz	drawbgtrlinelp
//			loop	drawbgtrlinelp
			pop	edi
		}
	}
}


INLINE void WinDraw_DrawPriLine(void)
{
	DWORD adr = ((VLINE+16)*1600+32);
	{
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			mov	edx, offset Grp_LineBufSP
			mov	ecx, TextDotX
		drawprilinelp:
			mov	ax, [edx]
			or	ax, ax
			jz	drawprilineskip
			mov	[ebx], ax
		drawprilineskip:
			add	edx, 2
			add	ebx, 2
//			dec	cx
//			jnz	drawprilinelp
			loop	drawprilinelp
		}
	}
}


INLINE void WinDraw_DrawTRLine(void)
{
	DWORD adr = ((VLINE+16)*1600+32);
		__asm {
			push	edi
			mov	ebx, adr
			add	ebx, offset ScrBuf
			xor	edx, edx
			xor	edi, edi
			xor	eax, eax
			mov	ecx, TextDotX
		drawtrlinelp:
			mov	di, word ptr Grp_LineBufSP[edx]
			or	di, di
			jz	drawtrlineskip

//		drawtrlineo:
#if 1						// ねこーねこー
			mov	ax, [ebx]
			and	di, Pal_HalfMask
			test	ax, Ibit
			jz	drawtrlineoI
			add	di, Pal_Ix2
		drawtrlineoI:
			and	ax, Pal_HalfMask
			add	ax, di		// 17bit計算中
			rcr	ax, 1		// 17bit計算中
#else
			push	ecx
			mov	cx, [ebx]
			and	cx, WinDraw_Pal16R
			and	di, WinDraw_Pal16R
			add	ecx, edi
			shr	ecx, 1
			and	cx, WinDraw_Pal16R
			mov	ax, cx
			mov	di, word ptr Grp_LineBufSP[edx]
			mov	cx, [ebx]
			and	cx, WinDraw_Pal16G
			and	di, WinDraw_Pal16G
			add	ecx, edi
			shr	ecx, 1
			and	cx, WinDraw_Pal16G
			or	ax, cx
			mov	di, word ptr Grp_LineBufSP[edx]
			mov	cx, [ebx]
			and	cx, WinDraw_Pal16B
			and	di, WinDraw_Pal16B
			add	ecx, edi
			shr	ecx, 1
			and	cx, WinDraw_Pal16B
			or	ax, cx
			mov	cx, [ebx]
			and	cx, Ibit
			or	ax, cx
			pop	ecx
#endif
			mov	[ebx], ax
		drawtrlineskip:
			add	edx, 2
			add	ebx, 2
			dec	cx
			jnz	drawtrlinelp
//			loop	drawtrlinelp
			pop	edi
		}
}


void WinDraw_DrawLine(void)
{
	int opaq, ton=0, gon=0, bgon=0, tron=0, pron=0, tdrawed=0, gdrawed=0;

	if (VLINE >= _countof(TextDirtyLine)) return;
	if (!TextDirtyLine[VLINE]) return;
	TextDirtyLine[VLINE] = 0;
	Draw_DrawFlag = 1;
#ifdef SUPPORT_32BPP
	ScrDirtyLine[VLINE] = 1;
#endif	// SUPPORT_32BPP


	if (Debug_Grp)
	{
	switch(VCReg0[1]&3)
	{
	case 0:					// 16 colors
		if (VCReg0[1]&4)		// 1024dot
		{
			if (VCReg2[1]&0x10)
			{
				if ( (VCReg2[0]&0x14)==0x14 )
				{
					Grp_DrawLine4hSP();
					pron = tron = 1;
				}
				else
				{
					Grp_DrawLine4h();
					gon=1;
				}
			}
		}
		else				// 512dot
		{
			if ( (VCReg2[0]&0x10)&&(VCReg2[1]&1) )
			{
				Grp_DrawLine4SP((VCReg1[1]   )&3/*, 1*/);			// 半透明の下準備
				pron = tron = 1;
			}
			opaq = 1;
			if (VCReg2[1]&8)
			{
				Grp_DrawLine4((VCReg1[1]>>6)&3, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&4)
			{
				Grp_DrawLine4((VCReg1[1]>>4)&3, opaq);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&2)
			{
				if ( ((VCReg2[0]&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine4TR((VCReg1[1]>>2)&3, opaq);
				else
					Grp_DrawLine4((VCReg1[1]>>2)&3, opaq);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&1)
			{
//				if ( (VCReg2[0]&0x1e)==0x1e )
//				{
//					Grp_DrawLine4SP((VCReg1[1]   )&3, opaq);
//					tron = pron = 1;
//				}
//				else
				if ( (VCReg2[0]&0x14)!=0x14 )
				{
					Grp_DrawLine4((VCReg1[1]   )&3, opaq);
					gon=1;
				}
			}
		}
		break;
	case 1:	
	case 2:	
		opaq = 1;		// 256 colors
		if ( (VCReg1[1]&3) <= ((VCReg1[1]>>4)&3) )	// 同じ値の時は、GRP0が優先（ドラスピ）
		{
			if ( (VCReg2[0]&0x10)&&(VCReg2[1]&1) )
			{
				Grp_DrawLine8SP(0);			// 半透明の下準備
				tron = pron = 1;
			}
			if (VCReg2[1]&4)
			{
				if ( ((VCReg2[0]&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine8TR(1, 1);
				else
					Grp_DrawLine8(1, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&1)
			{
				if ( (VCReg2[0]&0x14)!=0x14 )
				{
					Grp_DrawLine8(0, opaq);
					gon=1;
				}
			}
		}
		else
		{
			if ( (VCReg2[0]&0x10)&&(VCReg2[1]&1) )
			{
				Grp_DrawLine8SP(1);			// 半透明の下準備
				tron = pron = 1;
			}
			if (VCReg2[1]&4)
			{
				if ( ((VCReg2[0]&0x1e)==0x1e)&&(tron) )
					Grp_DrawLine8TR(0, 1);
				else
					Grp_DrawLine8(0, 1);
				opaq = 0;
				gon=1;
			}
			if (VCReg2[1]&1)
			{
				if ( (VCReg2[0]&0x14)!=0x14 )
				{
					Grp_DrawLine8(1, opaq);
					gon=1;
				}
			}
		}
		break;
	case 3:					// 65536 colors
		if (VCReg2[1]&15)
		{
			if ( (VCReg2[0]&0x14)==0x14 )
			{
				Grp_DrawLine16SP();
				tron = pron = 1;
			}
			else
			{
				Grp_DrawLine16();
				gon=1;
			}
		}
		break;
	}
	}


//	if ( ( ((VCReg1[0]&0x30)>>4) < (VCReg1[0]&0x03) ) && (gon) )
//		gdrawed = 1;				// GrpよりBGの方が上

	if ( ((VCReg1[0]&0x30)>>2) < (VCReg1[0]&0x0c) )
	{						// BGの方が上
		if ((VCReg2[1]&0x20)&&(Debug_Text))
		{
			Text_DrawLine(1);
			ton = 1;
		}
		else
			ZeroMemory(Text_TrFlag, TextDotX+16);

		if ((VCReg2[1]&0x40)&&(BG_Regs[8]&2)&&(!(BG_Regs[0x11]&2))&&(Debug_Sp))
		{
			int s1, s2;
			s1 = (((BG_Regs[0x11]  &4)?2:1)-((BG_Regs[0x11]  &16)?1:0));
			s2 = (((CRTC_Regs[0x29]&4)?2:1)-((CRTC_Regs[0x29]&16)?1:0));
			VLINEBG = VLINE;
			VLINEBG <<= s1;
			VLINEBG >>= s2;
			if ( !(BG_Regs[0x11]&16) ) VLINEBG -= ((BG_Regs[0x0f]>>s1)-(CRTC_Regs[0x0d]>>s2));
			BG_DrawLine(!ton, 0);
			bgon = 1;
		}
	}
	else
	{						// Textの方が上
		if ((VCReg2[1]&0x40)&&(BG_Regs[8]&2)&&(!(BG_Regs[0x11]&2))&&(Debug_Sp))
		{
			int s1, s2;
			s1 = (((BG_Regs[0x11]  &4)?2:1)-((BG_Regs[0x11]  &16)?1:0));
			s2 = (((CRTC_Regs[0x29]&4)?2:1)-((CRTC_Regs[0x29]&16)?1:0));
			VLINEBG = VLINE;
			VLINEBG <<= s1;
			VLINEBG >>= s2;
			if ( !(BG_Regs[0x11]&16) ) VLINEBG -= ((BG_Regs[0x0f]>>s1)-(CRTC_Regs[0x0d]>>s2));
			ZeroMemory(Text_TrFlag, TextDotX+16);
			BG_DrawLine(1, 1);
			bgon = 1;
		}
		else
		{
			if ((VCReg2[1]&0x20)&&(Debug_Text))
			{
				_asm{
					mov	ax, TextPal[0]
					shl	eax, 16
					mov	ax, TextPal[0]
					mov	edx, 16*2
					mov	ecx, TextDotX
					shr	ecx, 1
				BGLineClr_lp:
					mov	dword ptr BG_LineBuf[edx], eax
					add	edx, 4
					loop	BGLineClr_lp
				}
			} else {		// 20010120 （琥珀色）
				_asm{
					xor	eax, eax
					mov	edx, 16*2
					mov	ecx, TextDotX
					shr	ecx, 1
				BGLineClr_lp2:
					mov	dword ptr BG_LineBuf[edx], eax
					add	edx, 4
					loop	BGLineClr_lp2
				}
			}
			ZeroMemory(Text_TrFlag, TextDotX+16);
			bgon = 1;
		}

		if ((VCReg2[1]&0x20)&&(Debug_Text))
		{
			Text_DrawLine(!bgon);
			ton = 1;
		}
	}


	opaq = 1;


#if 0
					// Pri = 3（違反）に設定されている画面を表示
		if ( ((VCReg1[0]&0x30)==0x30)&&(bgon) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x03)&&(tron) )
			{
				if ( (VCReg1[0]&3)<((VCReg1[0]>>2)&3) )
				{
					WinDraw_DrawBGLineTR(opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				WinDraw_DrawBGLine(opaq, /*tdrawed*/0);
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( ((VCReg1[0]&0x0c)==0x0c)&&(ton) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x0c)&&(tron) )
				WinDraw_DrawTextLineTR(opaq);
			else
				WinDraw_DrawTextLine(opaq, /*tdrawed*/((VCReg1[0]&0x30)==0x30));
			opaq = 0;
			tdrawed = 1;
		}
#endif
					// Pri = 2 or 3（最下位）に設定されている画面を表示
					// プライオリティが同じ場合は、GRP<SP<TEXT？（ドラスピ、桃伝、YsIII等）

					// GrpよりTextが上にある場合にTextとの半透明を行うと、SPのプライオリティも
					// Textに引きずられる？（つまり、Grpより下にあってもSPが表示される？）

					// KnightArmsとかを見ると、半透明のベースプレーンは一番上になるみたい…。

		if ( (VCReg1[0]&0x02) )
		{
			if (gon)
			{
				WinDraw_DrawGrpLine(opaq);
				opaq = 0;
			}
			if (tron)
			{
				WinDraw_DrawGrpLineNonSP(opaq);
				opaq = 0;
			}
		}
		if ( (VCReg1[0]&0x20)&&(bgon) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x02)&&(tron) )
			{
				if ( (VCReg1[0]&3)<((VCReg1[0]>>2)&3) )
				{
					WinDraw_DrawBGLineTR(opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				WinDraw_DrawBGLine(opaq, /*0*/tdrawed);
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( (VCReg1[0]&0x08)&&(ton) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&((VCReg1[0]&0x03)!=0x02)&&(tron) )
				WinDraw_DrawTextLineTR(opaq);
			else
				WinDraw_DrawTextLine(opaq, tdrawed/*((VCReg1[0]&0x30)>=0x20)*/);
			opaq = 0;
			tdrawed = 1;
		}

					// Pri = 1（2番目）に設定されている画面を表示
		if ( ((VCReg1[0]&0x03)==0x01)&&(gon) )
		{
			WinDraw_DrawGrpLine(opaq);
			opaq = 0;
		}
		if ( ((VCReg1[0]&0x30)==0x10)&&(bgon) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&(!(VCReg1[0]&0x03))&&(tron) )
			{
				if ( (VCReg1[0]&3)<((VCReg1[0]>>2)&3) )
				{
					WinDraw_DrawBGLineTR(opaq);
					tdrawed = 1;
					opaq = 0;
				}
			}
			else
			{
				WinDraw_DrawBGLine(opaq, ((VCReg1[0]&0xc)==0x8));
				tdrawed = 1;
				opaq = 0;
			}
		}
		if ( ((VCReg1[0]&0x0c)==0x04) && ((VCReg2[0]&0x5d)==0x1d) && (VCReg1[0]&0x03) && (((VCReg1[0]>>4)&3)>(VCReg1[0]&3)) && (bgon) && (tron) )
		{
			WinDraw_DrawBGLineTR(opaq);
			tdrawed = 1;
			opaq = 0;
			if (tron)
			{
				WinDraw_DrawGrpLineNonSP(opaq);
			}
		}
		else if ( ((VCReg1[0]&0x03)==0x01)&&(tron)&&(gon)&&(VCReg2[0]&0x10) )
		{
			WinDraw_DrawGrpLineNonSP(opaq);
			opaq = 0;
		}
		if ( ((VCReg1[0]&0x0c)==0x04)&&(ton) )
		{
			if ( ((VCReg2[0]&0x5d)==0x1d)&&(!(VCReg1[0]&0x03))&&(tron) )
				WinDraw_DrawTextLineTR(opaq);
			else
				WinDraw_DrawTextLine(opaq, ((VCReg1[0]&0x30)>=0x10));
			opaq = 0;
			tdrawed = 1;
		}

					// Pri = 0（最優先）に設定されている画面を表示
		if ( (!(VCReg1[0]&0x03))&&(gon) )
		{
			WinDraw_DrawGrpLine(opaq);
			opaq = 0;
		}
		if ( (!(VCReg1[0]&0x30))&&(bgon) )
		{
			WinDraw_DrawBGLine(opaq, /*tdrawed*/((VCReg1[0]&0xc)>=0x4));
			tdrawed = 1;
			opaq = 0;
		}
		if ( (!(VCReg1[0]&0x0c)) && ((VCReg2[0]&0x5d)==0x1d) && (((VCReg1[0]>>4)&3)>(VCReg1[0]&3)) && (bgon) && (tron) )
		{
			WinDraw_DrawBGLineTR(opaq);
			tdrawed = 1;
			opaq = 0;
			if (tron)
			{
				WinDraw_DrawGrpLineNonSP(opaq);
			}
		}
		else if ( (!(VCReg1[0]&0x03))&&(tron)&&(VCReg2[0]&0x10) )
		{
			WinDraw_DrawGrpLineNonSP(opaq);
			opaq = 0;
		}
		if ( (!(VCReg1[0]&0x0c))&&(ton) )
		{
			WinDraw_DrawTextLine(opaq, 1);
			tdrawed = 1;
			opaq = 0;
		}

					// 特殊プライオリティ時のグラフィック
		if ( ((VCReg2[0]&0x5c)==0x14)&&(pron) )	// 特殊Pri時は、対象プレーンビットは意味が無いらしい（ついんびー）
		{
			WinDraw_DrawPriLine();
		}
		else if ( ((VCReg2[0]&0x5d)==0x1c)&&(tron) )	// 半透明時に全てが透明なドットをハーフカラーで埋める
		{						// （AQUALES）
			DWORD adr = ((VLINE+16)*1600+32);
			__asm {
				mov	ebx, adr
				add	ebx, offset ScrBuf
				xor	edx, edx
				mov	ecx, TextDotX
			trsublinelp:
				mov	ax, word ptr Grp_LineBufSP[edx]
				or	ax, ax
				jz	trsublineskip
				test	word ptr [ebx], 0ffffh
				jnz	trsublineskip
				and	ax, Pal_HalfMask
				shr	ax, 1		// 17bit計算中
				mov	[ebx], ax
			trsublineskip:
				add	edx, 2
				add	ebx, 2
				loop	trsublinelp
			}
		}


	if (opaq)
	{
		DWORD adr = ((VLINE+16)*1600+32);
		__asm {
			mov	ebx, adr
			add	ebx, offset ScrBuf
			mov	ecx, TextDotX
			shr	cx, 1
		clrscrlp:
			mov	dword ptr [ebx], 0
			add	ebx, 4
//			dec	cx
//			jnz	clrscrlp
			loop	clrscrlp
		}
	}
}
