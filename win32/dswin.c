// ---------------------------------------------------------------------------
// 　OPMDRV2... DSOUND3.CPP
// ---------------------------------------------------------------------------

#include	<windows.h>
#include	<dsound.h>
#include	"common.h"
#include	"dswin.h"
#include	"prop.h"
//#include	"../x68k/opm.h"
#include	"../x68k/adpcm.h"
#include	"../x68k/mercury.h"
#include "../fmgen/fmg_wrap.h"

//	DSBUFFERDESC構造体サイズをDirectX3の物にしないと
//	いくらDirectX3機能を使用してたとしても 動かない。

#if 1
	#define	DSBUFFERDESC_SIZE	20			// DirectX3 Structsize
#else
	#define	DSBUFFERDESC_SIZE	sizeof(DSBUFFERDESC)
#endif

#ifndef DSBVOLUME_MAX
#define	DSBVOLUME_MAX			0
#endif
#ifndef DSBVOLUME_MIN
#define	DSBVOLUME_MIN			(-10000)
#endif


#define RELEASE(x) 	if (x) {IDirectSoundBuffer_Release(x); (x) = NULL;}

static	LPDIRECTSOUND		pDSound = NULL;
static	LPDIRECTSOUNDBUFFER	pDSData3 = NULL;
static	BYTE			DSData3Event = 0xff;


extern	HWND	hWndMain;

	short	playing = FALSE;

	BYTE	pcmbuffer[2*2*48000];
	BYTE	*pcmbufp = pcmbuffer;
	DWORD	pcmfreemax = 22050;
	DWORD	pcmfree = 22050;
	DWORD	ds_halfbuffer = 22050 * 4;
	DWORD	ratebase1000 = 22;
	DWORD	ratebase = 22050;
	long	DSound_PreCounter = 0;

// ぷろとたいぷ
void dx3_buffer_out(DWORD pos);

// ---------------------------------------------------------------------------
//  DirectSound3の初期化
// ---------------------------------------------------------------------------

int DSound_Init(DWORD rate, DWORD buflen) {

	DSBUFFERDESC		dsbdesc;
	PCMWAVEFORMAT		pcmwf;
	LPBYTE				lpBlockAdd1;
	LPBYTE				lpBlockAdd2;
	DWORD				blockSize1;
	DWORD				blockSize2;

	// 初期化済みか？
	if (playing) {
		return(FALSE);
	}

	// 無音指定か？
	if (rate == 0) {
		pDSound = NULL;
		pDSData3 = NULL;
		return(TRUE);
	}

	// お弁当
	DSData3Event = 0xff;

	// pcmfreemax = Dsoundバッファの半分のサンプル数
	pcmfree = pcmfreemax = ((DWORD)rate * buflen / 200 / 2);
	ds_halfbuffer = pcmfreemax * 4;
	pcmbufp = pcmbuffer;
	ratebase1000 = rate / 1000;
	ratebase = rate;

	// DirectSoundの初期化
	if (FAILED(DirectSoundCreate(0, &pDSound, 0))) {
		RELEASE(pDSound);
		return(FALSE);
	}
	if (FAILED(IDirectSound_SetCooperativeLevel(pDSound, hWndMain,
														DSSCL_PRIORITY))) {
		if (FAILED(IDirectSound_SetCooperativeLevel(pDSound, hWndMain,
														DSSCL_NORMAL))) {
			RELEASE(pDSound);
			return(FALSE);
		}
	}

	// サウンドバッファを確保するよ〜
	ZeroMemory(&pcmwf, sizeof(PCMWAVEFORMAT));
	pcmwf.wf.wFormatTag = WAVE_FORMAT_PCM;
	pcmwf.wf.nChannels = 2;
	pcmwf.wf.nSamplesPerSec = rate;
	pcmwf.wBitsPerSample = 16;
	pcmwf.wf.nBlockAlign = 4;
	pcmwf.wf.nAvgBytesPerSec = rate * 4;

	ZeroMemory(&dsbdesc, sizeof(DSBUFFERDESC));
	dsbdesc.dwSize = DSBUFFERDESC_SIZE;
	dsbdesc.dwFlags = DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY | 
			  DSBCAPS_LOCSOFTWARE | DSBCAPS_STICKYFOCUS | DSBCAPS_GETCURRENTPOSITION2;
	dsbdesc.lpwfxFormat = (LPWAVEFORMATEX)&pcmwf;
	dsbdesc.dwBufferBytes = ds_halfbuffer * 2;
	if (FAILED(IDirectSound_CreateSoundBuffer(pDSound, &dsbdesc, &pDSData3,
																NULL))) {
		RELEASE(pDSound);
		return(FALSE);
	}

	// おっけーだから一応サウンドバッファ初期化するね〜
	if (SUCCEEDED(IDirectSoundBuffer_Lock(pDSData3, 0, ds_halfbuffer * 2,
								(LPVOID*)&lpBlockAdd1, &blockSize1,
								(LPVOID*)&lpBlockAdd2, &blockSize2, 0))) {
		ZeroMemory(lpBlockAdd1, blockSize1);
		if (lpBlockAdd2 != NULL) {
			ZeroMemory(lpBlockAdd2, blockSize2);
		}
		IDirectSoundBuffer_Unlock(pDSData3,
						lpBlockAdd1, blockSize1, lpBlockAdd2, blockSize2);
	}

	IDirectSoundBuffer_SetVolume(pDSData3, 0);

	playing = TRUE;
	return(TRUE);
}


void DSound_Play(void) {

	if (pDSData3 != NULL) {
		IDirectSoundBuffer_Play(pDSData3, 0, 0, DSBPLAY_LOOPING);
	}
}

void DSound_Stop(void) {

	if (pDSData3 != NULL) {
		IDirectSoundBuffer_Stop(pDSData3);
	}
}


// ---------------------------------------------------------------------------
//  後片付け
// ---------------------------------------------------------------------------

int DSound_Cleanup(void) {

	playing = FALSE;
	if (pDSData3 != NULL) {
		IDirectSoundBuffer_Stop(pDSData3);
	}

	RELEASE(pDSData3);
	RELEASE(pDSound);
	return(TRUE);
}


// ---------------------------------------------------------------------------
//  適当に呼ばれる
// ---------------------------------------------------------------------------

void FASTCALL DSound_Send0(long clock)
{
	int length = 0;
	if (pDSData3 != NULL) {
		DSound_PreCounter += (ratebase*clock);
		while(DSound_PreCounter>=10000000L)
		{
			length ++;
			DSound_PreCounter -= 10000000L;
		}
		if (length) {
			if ((long)pcmfree >= length) {
				ADPCM_Update((short *)pcmbufp, length);
				OPM_Update((short *)pcmbufp, length);
				Mcry_Update((short *)pcmbufp, length);
				pcmbufp += length * sizeof(WORD) * 2;
				pcmfree -= length;
			} else {
				ADPCM_Update((short *)pcmbufp, pcmfree);
				OPM_Update((short *)pcmbufp, pcmfree);
				Mcry_Update((short *)pcmbufp, pcmfree);
				pcmbufp += pcmfree * sizeof(WORD) * 2;
				pcmfree = 0;
			}
		}
	}
}


void FASTCALL DSound_Send(void)
{
	DWORD	pos, wpos;

	if (pDSData3 != NULL) {
		if (IDirectSoundBuffer_GetCurrentPosition(pDSData3, &pos, &wpos)
								== DS_OK) {
			if (pos >= ds_halfbuffer) {
				if (DSData3Event != 0) {
					DSData3Event = 0;
					ADPCM_Update((short *)pcmbufp, pcmfree);
					OPM_Update((short *)pcmbufp, pcmfree);
					Mcry_Update((short *)pcmbufp, pcmfree);
					dx3_buffer_out(0);
					// リセット
					pcmbufp = pcmbuffer;
					pcmfree = pcmfreemax;
				}
			}
			else
			{
				if (DSData3Event != 1) {
					DSData3Event = 1;
					ADPCM_Update((WORD *)pcmbufp, pcmfree);
					OPM_Update((WORD *)pcmbufp, pcmfree);
					Mcry_Update((WORD *)pcmbufp, pcmfree);
					dx3_buffer_out(ds_halfbuffer);
					// リセット
					pcmbufp = pcmbuffer;
					pcmfree = pcmfreemax;
				}
			}
		}
	}
}


// ---------------------------------------------------------------------------
//  DSoundバッファにデータを放りこむ
// ---------------------------------------------------------------------------

static void dx3_buffer_out(DWORD pos) {

	HRESULT	hr;
	LPBYTE	lpBlockAdd1;
	LPBYTE	lpBlockAdd2;
	DWORD	blockSize1;
	DWORD	blockSize2;

	if ((hr = IDirectSoundBuffer_Lock(pDSData3, pos, ds_halfbuffer,
								(LPVOID*)&lpBlockAdd1, &blockSize1,
								(LPVOID*)&lpBlockAdd2, &blockSize2, 0))
													== DSERR_BUFFERLOST) {
		IDirectSoundBuffer_Restore(pDSData3);
		hr = IDirectSoundBuffer_Lock(pDSData3, pos, ds_halfbuffer,
								(LPVOID*)&lpBlockAdd1, &blockSize1,
								(LPVOID*)&lpBlockAdd2, &blockSize2, 0);
	}
	if (SUCCEEDED(hr)) {
		CopyMemory(lpBlockAdd1, pcmbuffer, blockSize1);
		if (lpBlockAdd2 != NULL) {
			CopyMemory(lpBlockAdd2, pcmbuffer + blockSize1, blockSize2);
		}
		IDirectSoundBuffer_Unlock(pDSData3,
						lpBlockAdd1, blockSize1, lpBlockAdd2, blockSize2);
	}
}
