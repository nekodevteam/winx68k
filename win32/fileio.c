// ---------------------------------------------------------------------------------------
//  FILEIO.C - DOSIO.C中のファイル管理部だけ版
//  DOSIOほぼそのまま。
// ---------------------------------------------------------------------------------------
#include	"fileio.h"

extern char winx68k_dir[MAX_PATH];

char *getFileName(char *filename)
{
	char	*ret;

	ret = filename;
	while(*filename != '\0') {
		if (IsDBCSLeadByte(*filename) == 0) {
			if ((*filename == '\\') || (*filename == '/')
					|| (*filename == ':')) {
				ret = filename + 1;
			}
		}
		filename = CharNext(filename);
	}
	return(ret);
}


FILEH File_Open(BYTE *filename) {

	FILEH	ret;

	if ((ret = CreateFile((char *)filename, GENERIC_READ | GENERIC_WRITE,
						0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL))
											== INVALID_HANDLE_VALUE) {
		if ((ret = CreateFile((char *)filename, GENERIC_READ,
						0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL))
											== INVALID_HANDLE_VALUE) {
			return ((FILEH)FALSE);
		}
	}
	return(ret);
}

FILEH File_Create(BYTE *filename) {

	FILEH	ret;

	if ((ret = CreateFile((char *)filename, GENERIC_READ | GENERIC_WRITE,
						 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL))
									== INVALID_HANDLE_VALUE) {
		return ((FILEH)FALSE);
	}
	return(ret);
}

DWORD File_Seek(FILEH handle, long pointer, short mode) {

	return (SetFilePointer(handle, pointer, 0, mode));
}

DWORD File_Read(FILEH handle, void *data, DWORD length) {

	DWORD	readsize;

	if (!ReadFile(handle, data, length, &readsize, NULL)) {
		return(0);
	}
	return ((DWORD)readsize);
}

DWORD File_Write(FILEH handle, void *data, DWORD length) {

	DWORD	writesize;

	if (!WriteFile(handle, data, length, &writesize, NULL)) {
		return (0);
	}
	return ((DWORD)writesize);
}

short File_Close(FILEH handle) {

	CloseHandle(handle);
	return (TRUE);
}

short File_Attr(BYTE *filename) {

	return ((short)GetFileAttributes((char *)filename));
}


// EXEのあるDir中のファイルの操作

FILEH	File_OpenCurDir(BYTE *filename)
{
	char buf[MAX_PATH];
	strcpy(buf, winx68k_dir);
	strcat(buf, (char *)filename);
	return (File_Open(buf));
}

FILEH	File_CreateCurDir(BYTE *filename)
{
	char buf[MAX_PATH];
	strcpy(buf, winx68k_dir);
	strcat(buf, (char *)filename);
	return (File_Create(buf));
}

short	File_AttrCurDir(BYTE *filename)
{
	char buf[MAX_PATH];
	strcpy(buf, winx68k_dir);
	strcat(buf, (char *)filename);
	return (File_Attr(buf));
}
